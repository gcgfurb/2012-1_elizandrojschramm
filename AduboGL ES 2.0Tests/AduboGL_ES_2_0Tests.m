//
//  AduboGL_ES_2_0Tests.m
//  AduboGL ES 2.0Tests
//
//  Created by Elizandro Schramm on 13/06/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "AduboGL_ES_2_0Tests.h"

@implementation AduboGL_ES_2_0Tests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in AduboGL ES 2.0Tests");
}

@end
