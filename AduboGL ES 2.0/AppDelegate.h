//
//  AppDelegate.h
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 17/04/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
