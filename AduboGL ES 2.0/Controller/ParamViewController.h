//
//  ParamViewControllerViewController.h
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 16/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParamViewController : UIViewController {
    IBOutlet UIStepper *_red;
    IBOutlet UIStepper *_green;
    IBOutlet UIStepper *_blue;
    IBOutlet UILabel *_labelR;
    IBOutlet UILabel *_labelG;
    IBOutlet UILabel *_labelB;
    IBOutlet UILabel *_labelTitle;
    IBOutlet UISwitch *_depthTest;
    IBOutlet UILabel *_textDepthTest;
    IBOutlet UILabel *_textRed;
    IBOutlet UILabel *_textGreen;
    IBOutlet UILabel *_textBlue;
}

- (IBAction)stepperRedValueChanged:(id)sender;
- (IBAction)stepperGreenValueChanged:(id)sender;
- (IBAction)stepperBlueValueChanged:(id)sender;
- (IBAction)switchValueChanged:(id)sender;

@end
