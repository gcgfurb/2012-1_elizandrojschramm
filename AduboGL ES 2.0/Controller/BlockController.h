//
//  BlockController.h
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 14/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BlockView.h"
#import "BaseBlockView.h"

@interface BlockController : NSObject

+ (BlockController*)getInstance; //método estático para o Singleton
- (void)addBlockView:(BlockView*)aBlockView;
- (void)removeBlockView:(BlockView*)aBlockView;
- (BlockView*)moveBlockView:(UITouch*)aTouch:(CGPoint)aLocation;
- (BOOL)endMoveBlockView:(BlockView*)aBlockView;
- (BlockView*)getBlockByPosition:(CGPoint)aPoint;
- (NSMutableArray*)getBaseBlocks;
- (NSMutableArray*)getAllBlocks;
- (Block*)getBlockByClassName:(NSString*)aClassName;
- (void)clear;

/**
 * Guarda referência para o bloco selecionado.
 **/
@property BlockView* selectedBlock;

@end
