//
//  ParamViewControllerViewController.m
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 16/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "ParamViewController.h"
#import "BlockController.h"
#import "GLClearColorBlock.h"
#import "GLDepthTestBlock.h"

@interface ParamViewController ()

@end

@implementation ParamViewController {
    BlockController *_controller;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder: aDecoder];
    if (self) {
        _controller = [BlockController getInstance];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_labelTitle setBackgroundColor:[UIColor blackColor]];
    [_labelTitle setFont:[UIFont boldSystemFontOfSize:16.0]];
    [_labelTitle setTextAlignment:UITextAlignmentCenter];
    _labelTitle.textColor = [UIColor whiteColor];
    
    if (_controller.selectedBlock) {
        [_labelTitle setText:[_controller.selectedBlock.modelBlock getName]];
        NSString *xClassName = [_controller.selectedBlock.modelBlock getClassName];
        if (xClassName == @"GLClearColorBlock") {
            double step = 0.1;
            GLClearColorBlock *xBlock;
            xBlock = (GLClearColorBlock*)_controller.selectedBlock.modelBlock;
            //Stepper do vermelho
            _red.stepValue = step;
            _red.value = [xBlock getRed];
            _labelR.text = [NSString stringWithFormat:@"%.1f", _red.value];
            //Stepper do verde
            _green.stepValue = step;
            _green.value = [xBlock getGreen];
            _labelG.text = [NSString stringWithFormat:@"%.1f", _green.value];
            //Stepper do azul
            _blue.stepValue = step;
            _blue.value = [xBlock getBlue];
            _labelB.text = [NSString stringWithFormat:@"%.1f", _blue.value];
            //ocultar componentes de parâmetro do outro bloco
            _textDepthTest.hidden = YES;
            _depthTest.hidden = YES;
        } else if (xClassName == @"GLDepthTestBlock") {
            GLDepthTestBlock *xBlock;
            xBlock = (GLDepthTestBlock*)_controller.selectedBlock.modelBlock;
            [_depthTest setOn:[xBlock getEnableDepthTest]];
            //ocultar componentes de parâmetro do outro bloco
            _red.hidden = YES;
            _labelR.hidden = YES;
            _textRed.hidden = YES;
            _green.hidden = YES;
            _labelG.hidden = YES;
            _textGreen.hidden = YES;
            _blue.hidden = YES;
            _labelB.hidden = YES;
            _textBlue.hidden = YES;
        }
    }
}

- (void)updateColorBlockValue {
    [(GLClearColorBlock*)_controller.selectedBlock.modelBlock setColor:_red.value :_green.value :_blue.value];
}

- (IBAction)stepperRedValueChanged:(id)sender {
    _labelR.text = [NSString stringWithFormat:@"%.1f", _red.value];
    [self updateColorBlockValue];
}

- (IBAction)stepperGreenValueChanged:(id)sender {
    _labelG.text = [NSString stringWithFormat:@"%.1f", _green.value];
    [self updateColorBlockValue];
}

- (IBAction)stepperBlueValueChanged:(id)sender {
    _labelB.text = [NSString stringWithFormat:@"%.1f", _blue.value];
    [self updateColorBlockValue];
}

- (IBAction)switchValueChanged:(id)sender {
    [(GLDepthTestBlock*)_controller.selectedBlock.modelBlock setEnableDepthTest: _depthTest.on];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
