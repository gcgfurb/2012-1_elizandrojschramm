//
//  OpenGLController.m
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 15/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "OpenGLController.h"
#import "BlockController.h"
#import "OpenGLView.h"
#import "OpenGLProperties.h"

@implementation OpenGLController {
    BlockController *_blockController;
//    SEL vRenderOperation;
    CADisplayLink *_displayLink;
    Block *_renderBlock;
    BOOL _alreadyStarted;
    BOOL _firstTime; //apenas para os testes de performance
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder: aDecoder];
    if (self) {
        _blockController = [BlockController getInstance];
        _renderBlock = nil;
        _alreadyStarted = NO;
        _firstTime = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"Iniciando resultado");
    OpenGLView *xGLView = (OpenGLView*)self.view;
    //instanciar o objeto que será para as operações dos blocos
    OpenGLProperties* xGLProp = [[OpenGLProperties alloc] init];
    //percorrer todos os blocos base encaixados
    for (Block* xBlock in [_blockController getBaseBlocks]) {
        //executar as funções OpenGL ES de cada um deles
        [xBlock performSelector:xBlock.operation withObject:xGLProp];
    }
    //atribuir as propriedades que cada bloco valorou ao view
    xGLView.properties = xGLProp;
    //configurar o método que será executado a cada refresh da tela
    [self setupDisplayLink];
}

//- (void)viewDidAppear:(BOOL)animated {
//    NSLog(@"Resultado apareceu");
//}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear: animated];
    [_displayLink removeFromRunLoop: [NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
}

- (void)setupDisplayLink {
    _displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(render:)];
    [_displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];    
}

- (void)render:(CADisplayLink*)displayLink {
    if (!_alreadyStarted) {
        [(OpenGLView*)self.view start];
        _alreadyStarted = YES;
    }
    if (_renderBlock) {
        [_renderBlock performSelector: _renderBlock.operation];
        if (_firstTime) {
            NSLog(@"Primeira passada no render.");
            _firstTime = NO;
        }
    } else {
        //verifica se tem o bloco Render
        Block *xBlock = [_blockController getBlockByClassName:@"RenderBlock"];
        if (xBlock) {
            _renderBlock = xBlock;
        }
    }
    [(OpenGLView*)self.view render: displayLink];
}

@end
