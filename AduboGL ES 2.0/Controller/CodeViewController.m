//
//  CodeViewController.m
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 16/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "CodeViewController.h"
#import "BlockController.h"

@interface CodeViewController ()

@end

@implementation CodeViewController {
    BlockController *_controller;
}

/**
 * Inicializa o objeto.
 **/
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder: aDecoder];
    if (self) {
        _controller = [BlockController getInstance];
    }
    return self;
}

/**
 * Chamado quando a view acaba de ser carregada.
 * Neste método os componentes da tela são desenhados conforme o bloco selecionado.
 */
- (void)viewDidLoad {
    [super viewDidLoad];
    CGRect xRect = CGRectMake(0, 20, 600, 40);
    UITextView *xBlockName = [[UITextView alloc] initWithFrame: xRect];
    [xBlockName setBackgroundColor:[UIColor blackColor]];
    [xBlockName setFont:[UIFont boldSystemFontOfSize:16.0]];
    [xBlockName setTextAlignment:UITextAlignmentCenter];
    xBlockName.textColor = [UIColor whiteColor];
    [xBlockName setEditable:NO];
    [[self view] addSubview:xBlockName];
    
    if (_controller.selectedBlock) {
        [xBlockName setText: [_controller.selectedBlock.modelBlock getName]];        
        xRect = CGRectMake(0, 80, 600, 420);
        UITextView *xLabelCode = [[UITextView alloc] initWithFrame: xRect];
        [xLabelCode setFont:[UIFont boldSystemFontOfSize:14.0]];
        xLabelCode.textColor = [UIColor blackColor];
        [xLabelCode setEditable:NO];
        [[self view] addSubview:xLabelCode];
        [xLabelCode setText: [_controller.selectedBlock.modelBlock getReadableCode]];
    } else {
        [xBlockName setText: @"Selecione um bloco"];
    }

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
