//
//  BlockController.m
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 14/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "BlockController.h"
#import "BaseBlock.h"
#import "Connector.h"

static BlockController* defaultBlockController;
static const int positionOfBar = 350;

@class BlockController;

@implementation BlockController {
    NSMutableArray* _allBlocks;
    NSMutableArray* _baseBlocks;
    int _globalY;
}

@synthesize selectedBlock;

/**
 * Inicializa o objeto. Não deve ser chamado. O getInstance é que deve ser usado.
 **/
- (id)init {
    self = [super init];
    if (self) {
        _allBlocks = [[NSMutableArray alloc] init];
        _baseBlocks = [[NSMutableArray alloc] init];
        _globalY = 20;
    }
    return self;
}

/**
 * Usado para implementar o Singleton.
 * Ninguém pode chamar o init desta classe, apenas o getInstance.
 * @return Objeto único da classe BlockController.
 **/
+ (BlockController*)getInstance {
    if (!defaultBlockController) {
        defaultBlockController = [[BlockController alloc] init];
    }
    return defaultBlockController;
}

/**
 * Adiciona um bloco da visão na lista de blocos _allBlocks.
 **/
- (void)addBlockView:(BlockView*)aBlockView {
    [_allBlocks addObject:aBlockView];
}

- (BOOL)existsBlockBaseEquals:(BaseBlockView*)aBaseBlockView {
    for (BaseBlockView *xBlock in _baseBlocks) {
        if (aBaseBlockView != xBlock) { //não pode comparar ele com ele mesmo
            if ([aBaseBlockView.modelBlock.getName isEqualToString: xBlock.modelBlock.getName]) {
                return YES;
            }
        }
    }
    return NO;
}

/**
 * Remove o bloco de visão da tela e todos os blocos que nele estão conectados.
 * @param aBlockView o bloco que se deseja apagar.
 **/
- (void)removeBlockView:(BlockView*)aBlockView {
    if ([aBlockView isKindOfClass: BaseBlockView.class]) {
        //apagar os blocos que estão ligados a este bloco
        BaseBlockView* xBaseBlockView = (BaseBlockView*)aBlockView;
        for (BlockView *xBlockView in [xBaseBlockView getLinkedBlocks]) {
            [self removeBlockView: xBlockView];
        }
        //se existe um bloco base igual a esse, então não pode apagar os que podem ser filhos dele
        if (![self existsBlockBaseEquals: xBaseBlockView]) {
            //apagar os blocos que estão na área de montagem e não estão ligados a este bloco, mas podem ser filhos dele
            BlockView *xBlockView = nil;
            for (int i = 0; i < [_allBlocks count]; i++) {
                xBlockView = [_allBlocks objectAtIndex:i];
                if ([(BaseBlock*)aBlockView.modelBlock canReceiveThisAsChild: xBlockView.modelBlock]) {
                    [self removeBlockView: xBlockView];
                    i--;
                }
            }  
        }
    }
    [aBlockView remove];
    [_allBlocks removeObject:aBlockView];
    [_baseBlocks removeObject:aBlockView];

    [self reorganizeBlocks];
}

/**
 * Verifica se o bloco arrastado está se encaixando em outro bloco.
 * Isso é feito para gerar o efeito imã ao bloco quando se aproximar do ponto de engate de outro bloco.
 * @param aBaseBlock o bloco base da linha.
 * @param aBlockView o bloco que está sendo arrastado.
 * @return se o bloco está ou não se encaixando no bloco base.
 **/
- (BOOL)verifyIfFits:(BaseBlock*)aBaseBlock:(BlockView*)aBlockView {
    BOOL xResult = NO;
    if ([aBaseBlock canReceiveThisAsChild: aBlockView.modelBlock]) {
        //o bloco base aceita o bloco movimentado como seu filho,
        //então vê se estão próximos o suficiente para encaixar
        int i = 0;
        while ((i < [[aBaseBlock clamps] count]) && !xResult) {
            Connector *xClamp = [aBaseBlock getRelativeClamp:i];
            Connector *xCramp = aBlockView.modelBlock.cramp;
            //compara os pontos de conexão se estão próximos 10 pixels
            if ((!xClamp.used) && ((xClamp.x < xCramp.x + 10) &&
                (xClamp.x > xCramp.x - 10) && 
                (xClamp.y < xCramp.y + 10) && 
                (xClamp.y > xCramp.y - 10))) {
                CGPoint xDistCramp = aBlockView.modelBlock.distanceCramp;
                //gera o efeito imã
                [aBlockView updateLocationFromOrigin: xClamp.x - xDistCramp.x 
                                                    :xClamp.y - xDistCramp.y];
                aBlockView.center = aBlockView.modelBlock.center;
                xResult = YES; //ok, se encaixam
            }
            i++;
        }
    } else {
        //o bloco base da linha não aceita este bloco como filho,
        //mas pode ser que ele tenha um filho que seja base também
        Block *xBlock = nil;
        int i = 0;
        while ((i < [[aBaseBlock children] count]) && !xResult) {
            xBlock = [[aBaseBlock children] objectAtIndex: i];
            if ([xBlock isKindOfClass: [BaseBlock class]]) {
                //chamada recursiva para ver se o bloco se encaixa com
                //o filho deste bloco base
                xResult = [self verifyIfFits:(BaseBlock*)xBlock :aBlockView];
            }
            i++;
        }
    }
    return xResult;
}

/**
 * Percorre a lista de blocos verificando qual bloco o Aluno está arrastando.
 * Ao encontrar o bloco arrastado, a rotina atualiza os pontos de localização do bloco, chamando o método updateLocationFromCenter,
 * e faz com que a localização tocada pelo Aluno no momento se torne o centro do bloco (xBlock.center = aLocation).
 * Além desta atualização de localização do bloco, a rotina verifica se o bloco arrastado está se encaixando em outro bloco através
 * do método verifyIfFits.
 * @param aTouch objeto da tela tocado pelo Aluno.
 * @param aLocation localização do toque.
 * @return o bloco arrastado.
 **/
- (BlockView*)moveBlockView:(UITouch*)aTouch:(CGPoint)aLocation {
    BlockView* xResult = nil;
    BlockView* xBaseBlockView = nil;
    BaseBlock* xBaseBlock = nil;
    BlockView* xBlock = nil;
    int i = 0;
    while ((i < [_allBlocks count]) && !xResult) {
        xBlock = [_allBlocks objectAtIndex: i];
        if ([aTouch view] == xBlock) {
            xResult = xBlock;
            //considera-se que o local do toque vira o novo centro da figura
            [xBlock updateLocationFromCenter: aLocation.x :aLocation.y];
            //tratamento se a localização for na linha de um bloco base
            xBaseBlockView = [self getBlockByPosition: aLocation];
            if ((xBaseBlockView) && (xBaseBlockView != xBlock)) {
                xBaseBlock = (BaseBlock*)xBaseBlockView.modelBlock;
                //se não está mexendo na peça pai e a peça pai aceita essa aqui como
                //filha, então verifica se está se encaixando
                if (![self verifyIfFits:xBaseBlock :xBlock]) {
                    xBlock.center = aLocation;
                }
            } else {
                xBlock.center = aLocation;
            }
        }
        i++;
    } 
    return xResult;
}

/**
 * Verifica se o bloco pode se encaixar em outro bloco na bandeja.
 * Ele auxilia o método endMoveBlockView na efetivação e consistência da nova localização do bloco movimentado.
 * @param aBaseBlock o bloco base da linha, mas do modelo.
 * @param aBlockView o bloco que foi arrastado.
 * @param aBaseBlockView o bloco base da linha, mas da visão.
 * @return se o bloco pode ficar encaixado no bloco base.
 **/
- (BOOL)verifyIfCanFit:(BaseBlock*)aBaseBlock
                      :(BlockView*)aBlockView
                      :(BaseBlockView*)aBaseBlockView {
    BOOL xResult = NO;
    if ([aBaseBlock canReceiveThisAsChild: aBlockView.modelBlock]) {
        //o bloco base aceita o bloco como filho, ou seja, podem se encaixar
        Connector* xClamp = [aBaseBlock getClampByXY:aBlockView.modelBlock.cramp.x 
                                                    :aBlockView.modelBlock.cramp.y];
        //recuperado o ponto de engate do bloco base
        if (xClamp && !xClamp.used) {
            //encontrou um ponto de engate e ele está livre
            [aBaseBlock addChild: aBlockView.modelBlock];
            [aBaseBlockView addLinkedBlock: aBlockView]; 
            xClamp.used = YES; //indica que o engate está ocupado...
            xClamp.block = aBlockView.modelBlock;
            if ([aBlockView.modelBlock hasBaseBlock]) {
                //este bloco já estava conectado em outro bloco base, 
                //então é preciso desconectar
                [(BaseBlock*)aBlockView.modelBlock.baseBlock removeChild
                                                    :aBlockView.modelBlock :xClamp];
                [aBaseBlockView removeLinkedBlock: aBlockView];
            }
            aBlockView.modelBlock.baseBlock = aBaseBlock;
            xResult = YES;
        } else if ([aBlockView.modelBlock hasBaseBlock]) {
            //não encontrou um ponto de engate disponível nesta posição para se 
            //encaixar, mas como ele já tinha um bloco base, precisa liberar conexão
            [(BaseBlock*)aBlockView.modelBlock.baseBlock removeChild
                                                    :aBlockView.modelBlock :nil];
            [aBaseBlockView removeLinkedBlock: aBlockView];
            aBlockView.modelBlock.baseBlock = nil;
        }                               
        if (!xClamp) {
            //bloco não conseguiu se encaixar, mas como o bloco base aceita ele
            //como filho, então pode permanecer na bandeja
            xResult = YES;
        }
//    ...
        
    } else {
        //o bloco base da linha não aceita este bloco como filho, mas pode ser que ele tenha um filho que seja base também e o aceite
        Block *xBlock = nil;
        int i = 0;
        while ((i < [[aBaseBlock children] count]) && !xResult) {
            xBlock = [[aBaseBlock children] objectAtIndex: i];
            if ([xBlock isKindOfClass: [BaseBlock class]]) {
                BaseBlockView *xBlockView = nil;
                int j = 0;
                NSMutableArray *xLinkedBlocks = [aBaseBlockView getLinkedBlocks];
                while ((j < [xLinkedBlocks count]) && !xBlockView) {
                    xBlockView = [xLinkedBlocks objectAtIndex: j];
                    if (!(xBlockView.modelBlock == xBlock)) {
                        xBlockView = nil;
                    }
                    j++;
                }
                if (xBlockView) {
                    xResult = [self verifyIfCanFit:(BaseBlock*)xBlock :aBlockView :xBlockView];
                } else {
                    xResult = NO;
                }
                
            }
            i++;
        }
    }
    return xResult;
}

/** 
 * Este método é chamado quando o toque é solto, ou seja, quando efetivamente vai tentar deixar o bloco naquela posição.
 * Quando for um bloco base, ele será colocado no "cabeçalho" da linha.
 * @param aBlock o bloco que acaba de ser arrastado pelo Aluno.
 * @return se foi possível ou não soltar o bloco naquela posição.
 **/
- (BOOL)endMoveBlockView:(BlockView*)aBlock {
    BOOL xResult = NO;
    int index = [_baseBlocks indexOfObjectIdenticalTo: aBlock];
    if (index < INT32_MAX) {
        //está movendo um bloco pai que já está na área de montagem,
        //então ou ele volta para o lugar ou então reordena
        [self reorganizeBlocks];
        xResult = YES;
    } else if (aBlock.modelBlock.location.y < _globalY) {
        //está em uma área de um bloco pai,
        //então precisa ver se ele aceita este bloco como filho
        BaseBlockView *xBaseBlockView;
        xBaseBlockView = (BaseBlockView*)[self getBlockByPosition: 
                                          aBlock.modelBlock.location];
        if ((xBaseBlockView) && (xBaseBlockView != aBlock)) {
            BaseBlock* xBaseBlock = (BaseBlock*)xBaseBlockView.modelBlock;
            xResult = [self verifyIfCanFit: xBaseBlock :aBlock :xBaseBlockView];
        }
    } else if ([aBlock isKindOfClass: BaseBlockView.class]) {
        //é um bloco base novo
        int xCount = [_baseBlocks count];
        if (xCount > 0) { 
            //verifica se este bloco pode se encaixar como irmão no bloco base
            //pega o último bloco base adicionado
            BaseBlockView *xBlockView = [_baseBlocks objectAtIndex: xCount - 1];
            BaseBlock *xBaseBlock = (BaseBlock*)xBlockView.modelBlock;
            if ([xBaseBlock canReceiveThisAsSibling:aBlock.modelBlock]) {
                //pode se encaixar como irmão
                //então adiciona na lista do bloco já estava na bandeja
                [xBaseBlock addSibling:aBlock.modelBlock];
                //para poder atualizar quando mover o bloco base
                [xBlockView addLinkedBlock:aBlock];
                xResult = YES;
            } //senão xResult continua NO e o ViewController remove o bloco
        } else {
            xResult = YES;
        }
        if (xResult) {
            //vira o bloco pai de uma área
            [self sendBlockToOriginPoint:aBlock];
            [_baseBlocks addObject:aBlock];
        }
    }
    return xResult;
}

/**
 * Retorna o bloco base da linha pelas coordenadas da posição desejada.
 **/
- (BlockView*)getBlockByPosition:(CGPoint)aPoint {
    for (BlockView *xBlock in _baseBlocks) {
        if ((aPoint.y >= xBlock.modelBlock.location.y) && (aPoint.y <= (xBlock.modelBlock.location.y + xBlock.getHeight))) {
            return xBlock;
        }
    }
    return nil;
}

/**
 * Envia o bloco base para sua posição original. Isto porquê os blocos base não podem ficar solto em qualquer lugar na bandeja.
 **/
-(void)sendBlockToOriginPoint:(BlockView*)aBlock {
    [aBlock updateLocationFromOrigin: positionOfBar :_globalY];
    aBlock.center = aBlock.modelBlock.center;
    _globalY = _globalY + aBlock.getHeight;
}

/**
 * Organiza a posição dos blocos base na bandeja.
 **/
- (void)reorganizeBlocks {
    _globalY = 20;
    for (BlockView *xBlock in _baseBlocks) {
        [self sendBlockToOriginPoint:xBlock];
    }
}

/**
 * Retorna os blocos base.
 **/
- (NSMutableArray*)getBaseBlocks {
    NSMutableArray *xResult = [[NSMutableArray alloc] init];
    for (BlockView *xBlockView in _baseBlocks) {
        [xResult addObject:xBlockView.modelBlock];
    }
    return xResult;
}

/**
 * Retorna todos os blocos inseridos na bandeja.
 **/
- (NSMutableArray*)getAllBlocks {
    return _allBlocks;
}

/**
 * Retorna o bloco pelo nome da classe.
 **/
- (Block*)getBlockByClassName:(NSString*)aClassName {
    for (BlockView *xBlockView in _allBlocks) {
        if ([xBlockView.modelBlock getClassName] == aClassName) {
            return xBlockView.modelBlock;
        }
    }
    return nil;
}

/**
 * Limpa a bandeja removendo todos os blocos dela.
 */
- (void)clear {
    if ([_baseBlocks count] > 0) {
        [self removeBlockView:[_baseBlocks objectAtIndex:0]];
    }
}

@end
