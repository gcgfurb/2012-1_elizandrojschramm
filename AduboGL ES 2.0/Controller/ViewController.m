//
//  ViewController.m
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 17/04/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "ViewController.h"
#import "BlockController.h"
#import "GLContextBlock.h"
#import "OpenGLES2Block.h"
#import "GLProgramBlock.h"
#import "VertexShaderBlock.h"
#import "FragmentShaderBlock.h"
#import "MainBlock.h"
#import "CompileVertexShaderBlock.h"
#import "CompileFragmentShaderBlock.h"
#import "LinkAttributesBlock.h"
#import "LinkGLProgramBlock.h"
#import "RenderBlock.h"
#import "GLClearColorBlock.h"
#import "GLDepthTestBlock.h"
#import <QuartzCore/QuartzCore.h>
#import "Connector.h"
#import "BrowsingFileAlertView.h"
#include <sys/sysctl.h>  
#include <mach/mach.h>

@interface ViewController ()
@end

static const int positionOfBar = 350;

@implementation ViewController {
    BlockController *_controller;
    BlockView *_GLContext, *_OpenGLES2;
    BlockView *_GLProgram, *_vertexShader, *_fragmentShader;
    BlockView *_main, *_compileVertexShader, *_compileFragmentShader, *_linkAttributes, *_linkGLProgram;
    BlockView *_render, *_GLClearColor, *_GLDepthTest;
    BlockView *_lastBlockMoved;
    BOOL _isCenter, _isClass, _isParams;
    CGPoint _center;
    BlockView *_lastBlockLoaded;
    BOOL _forSave;
    NSString *_xmlToSave;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {        
        _controller = [BlockController getInstance];
    }
    return self;
}

- (BlockView*)createBlockView:(NSString*)aImageName
                             :(CGRect)aRect
                             :(CGPoint)aDistanceCramp
                             :(Class)aClassOfBlockModel {
    BlockView *xResult;
    xResult = [[BlockView alloc] initWithFrame:aRect 
                                              :aImageName 
                                              :aClassOfBlockModel];
    
    xResult.modelBlock.distanceCramp = aDistanceCramp;
    [xResult.modelBlock updateCramp];
    return xResult;
}

- (BaseBlockView*)createBaseBlockView:(NSString*)aImageName:(CGRect)aRect
                                     :(NSArray*)aClamps
                                     :(Class)aClassOfBaseBlockModel {    
    BaseBlockView *xResult;
    xResult = [[BaseBlockView alloc] initWithFrame:aRect 
                                                  :aImageName
                                                  :aClassOfBaseBlockModel];
    
    for (Connector* xClamp in aClamps) {
        [(BaseBlock*)xResult.modelBlock addClamp: xClamp];
    }
    return xResult;
}

- (BaseBlockView*)createIntermediateBlockView:(NSString*)aImageName:(CGRect)aRect:(NSArray*)aClamps:(CGPoint)aDistanceCramp:(Class)aClassOfBaseBlockModel {
    BaseBlockView* xResult = [[BaseBlockView alloc] initWithFrame: aRect :aImageName :aClassOfBaseBlockModel];
    for (Connector* xClamp in aClamps) {
        [(BaseBlock*)xResult.modelBlock addClamp: xClamp];
    }
    xResult.modelBlock.distanceCramp = aDistanceCramp;
    [xResult.modelBlock updateCramp];
    return xResult;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches]anyObject];
    BlockView *xNewBlock = nil;
    
    if ([touch view] == _GLContext) {
        NSMutableArray* xConnectors = [[NSMutableArray alloc] initWithObjects:
                                       [[Connector alloc] init:35 : 31], nil];
        xNewBlock = [self createBaseBlockView:_GLContext.imageName 
                                             :_GLContext.frame :xConnectors 
                                             :GLContextBlock.class];
    } else if ([touch view] == _OpenGLES2) {
        xNewBlock = [self createBlockView:_OpenGLES2.imageName 
                                         :_OpenGLES2.frame :CGPointMake(-64, 13) 
                                         :OpenGLES2Block.class];
    } else if ([touch view] == _GLProgram) {
        NSMutableArray* xConnectors = [[NSMutableArray alloc] initWithObjects: 
                                       [[Connector alloc] init:45 :26], 
                                       [[Connector alloc] init:45 :83], nil];
        xNewBlock = [self createBaseBlockView:_GLProgram.imageName 
                                             :_GLProgram.frame :xConnectors 
                                             :GLProgramBlock.class];
    } else if ([touch view] == _vertexShader) {
//        ...
        
        xNewBlock = [self createBlockView:_vertexShader.imageName :_vertexShader.frame :CGPointMake(-53, 20) :VertexShaderBlock.class];
    } else if ([touch view] == _fragmentShader) {
        xNewBlock = [self createBlockView:_fragmentShader.imageName :_fragmentShader.frame :CGPointMake(-53, 20) :FragmentShaderBlock.class];
    } else if ([touch view] == _main) {
        NSMutableArray* xConnectors = [[NSMutableArray alloc] initWithObjects: [[Connector alloc] init:50 :20], 
                                                                               [[Connector alloc] init:50 :50],
                                                                               [[Connector alloc] init:70 :80],
                                                                               [[Connector alloc] init:85 :98],
                                                                               [[Connector alloc] init:95 :170], nil];
        xNewBlock = [self createBaseBlockView:_main.imageName :_main.frame :xConnectors :MainBlock.class];
    } else if ([touch view] == _compileVertexShader) {
        xNewBlock = [self createBlockView:_compileVertexShader.imageName :_compileVertexShader.frame :CGPointMake(-49, 12) :CompileVertexShaderBlock.class];
    } else if ([touch view] == _compileFragmentShader) {
        xNewBlock = [self createBlockView:_compileFragmentShader.imageName :_compileFragmentShader.frame :CGPointMake(-49, 12) :CompileFragmentShaderBlock.class];
    } else if ([touch view] == _linkAttributes) {
        xNewBlock = [self createBlockView:_linkAttributes.imageName :_linkAttributes.frame :CGPointMake(-13, 1) :LinkAttributesBlock.class];
    } else if ([touch view] == _linkGLProgram) {
        xNewBlock = [self createBlockView:_linkGLProgram.imageName :_linkGLProgram.frame :CGPointMake(-30, 13) :LinkGLProgramBlock.class];
    } else if ([touch view] == _render) {
        NSMutableArray* xConnectors = [[NSMutableArray alloc] initWithObjects: [[Connector alloc] init:34 :29], 
                                                                               [[Connector alloc] init:34 :59], nil];
        xNewBlock = [self createIntermediateBlockView:_render.imageName :_render.frame :xConnectors :CGPointMake(-17, 39) :RenderBlock.class];
    } else if ([touch view] == _GLClearColor) {
        xNewBlock = [self createBlockView:_GLClearColor.imageName :_GLClearColor.frame :CGPointMake(-24, 19) :GLClearColorBlock.class];
    } else if ([touch view] == _GLDepthTest){
        xNewBlock = [self createBlockView:_GLDepthTest.imageName :_GLDepthTest.frame :CGPointMake(-24, 19) :GLDepthTestBlock.class];
    }
    
    if (xNewBlock) {
        [_controller addBlockView:xNewBlock];
        [self.view addSubview:xNewBlock];
    }
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches]anyObject];
    CGPoint location = [touch locationInView: self.view];
    _lastBlockMoved = [_controller moveBlockView:touch :location];
}

- (void)selectBlock:(BlockView*)aBlockView {
    if (_controller.selectedBlock) {
        //des-selecionar o bloco atual se existe
        BlockView* xSel = _controller.selectedBlock;
        xSel.imageName = [xSel.imageName stringByReplacingOccurrencesOfString:@"Sel." withString:@"."];
        xSel.image = [UIImage imageNamed: xSel.imageName];
    } if (aBlockView) {
        //selecionar o bloco atual
        _controller.selectedBlock = aBlockView;
        aBlockView.imageName = [aBlockView.imageName stringByReplacingOccurrencesOfString:@"." withString:@"Sel."];
        aBlockView.image = [UIImage imageNamed: aBlockView.imageName];
        [self.view bringSubviewToFront:aBlockView];
        
        //se o bloco selecionado não possuir código, então oculta o botão Código, caso contrário, exibe o botão.
        if ([_controller.selectedBlock.modelBlock getReadableCode] == @"") {
            [_codeButton setHidden:YES];
        } else {
            [_codeButton setHidden:NO];
        }
        
        //apenas os blocos GLClearColor e GLDepthTest possuem parâmetros, então só para eles o botão "Parâmetro" é exibido.
        if ([_controller.selectedBlock.modelBlock getClassName] == @"GLClearColorBlock" || 
            [_controller.selectedBlock.modelBlock getClassName] == @"GLDepthTestBlock") {
            [_paramButton setHidden:NO];
        } else {
            [_paramButton setHidden:YES];
        }
    } else {
        //nenhum bloco selecionado
        [_codeButton setHidden:YES];
        [_paramButton setHidden:YES];
    }
    
}


-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [[event allTouches]anyObject];
    if (_lastBlockMoved) {
        //um bloco acabou de ser movimentado pelo Aluno, se ele foi solto 
        //depois da barra então passa pro BlockController efetivar a movimentação
        if (!((_lastBlockMoved.center.x > positionOfBar) 
              && [_controller endMoveBlockView:_lastBlockMoved])) {
            //ou foi solto antes da barra ou o BlockController não conseguiu
            //efetivar a movimentação pelo bloc ter sido solto onde não poderia
            [_controller removeBlockView:_lastBlockMoved]; //remove então este bloco
            [self selectBlock: nil]; //e seleciona nenhum bloco
        } else {
            //tudo certo: soltou o bloco onde poderia ter soltado
            [self selectBlock: _lastBlockMoved]; //seleciona este bloco
        }
        _lastBlockMoved = nil; //zera a referência porquê o último bloco movimentado
                               //já foi efetivado ou removido
    } else {
        //já que nenhum bloco foi movimentado, então verifica se o Aluno tocou
        //sobre algum bloco para selecioná-lo
        BlockView *xBlockView = nil;
        int i = 0;
        while ((i < [[_controller getAllBlocks] count]) && !xBlockView) {
            xBlockView = [[_controller getAllBlocks] objectAtIndex: i];
            //percorre todos os blocos do BlockController e compara com a área
            //tocada ne tela
            if ([touch view] != xBlockView) {
                //seta nil para continuar no laço do while
                xBlockView = nil;
            }
            i++;
        }
        if (xBlockView) {
            //se o toque foi dado sobre um bloco, então seleciona ele
            [self selectBlock: xBlockView];
        } else {
            //senão, seleciona nenhum bloco
            [self selectBlock: nil];
        }
    }
}

- (void)viewDidLoad {
    [_codeButton setHidden:YES];
    [_paramButton setHidden:YES];
    [super viewDidLoad];
    
    CGRect xRect;
    NSString *xImageName;
    double x = 20, y = 20, space = 10;
    xRect = CGRectMake(x, y, 124, 82);
    xImageName = @"ImgGLContext.png";
    _GLContext = [[BlockView alloc] initWithFrame: xRect :xImageName];
    [self.view addSubview:_GLContext];
    
    y = y + 82 + 5;
    xRect = CGRectMake(x, y, 148, 143);
    xImageName = @"ImgGLProgram.png";
    _GLProgram = [[BlockView alloc] initWithFrame: xRect :xImageName];
    [self.view addSubview:_GLProgram];
    
    y = y + 143 + 5;
    xRect = CGRectMake(x, y, 148, 208);
    xImageName = @"ImgMain.png";
    _main = [[BlockView alloc] initWithFrame: xRect :xImageName];
    [self.view addSubview:_main];
//...
    
    x = x + 148 + (space * 2); //iniciando a coluna do lado agora
    y = 20; //voltando à primeira linha
    xRect = CGRectMake(x, y, 122, 25);
    xImageName = @"ImgOpenGLES2.png";
    _OpenGLES2 = [[BlockView alloc] initWithFrame: xRect :xImageName];
    [self.view addSubview:_OpenGLES2];
    
    y = y + 25 + space;
    xRect = CGRectMake(x, y, 96, 56);
    xImageName = @"ImgVertexShader.png";
    _vertexShader = [[BlockView alloc] initWithFrame: xRect :xImageName];
    [self.view addSubview:_vertexShader];
    
    y = y + 56 + space;
    xRect = CGRectMake(x, y, 96, 56);
    xImageName = @"ImgFragmentShader.png";
    _fragmentShader = [[BlockView alloc] initWithFrame: xRect :xImageName];
    [self.view addSubview: _fragmentShader];
    
    y = y + 56 + space;
    xRect = CGRectMake(x, y, 131, 25);
    xImageName = @"ImgCompileVS.png";
    _compileVertexShader = [[BlockView alloc] initWithFrame: xRect :xImageName];
    [self.view addSubview: _compileVertexShader];
    
    y = y + 25 + space;
    xRect = CGRectMake(x, y, 131, 25);
    xImageName = @"ImgCompileFS.png";
    _compileFragmentShader = [[BlockView alloc] initWithFrame: xRect :xImageName];
    [self.view addSubview: _compileFragmentShader];
    
    y = y + 25 + space;
    xRect = CGRectMake(x, y, 131, 24);
    xImageName = @"ImgLinkGLProgram.png";
    _linkGLProgram = [[BlockView alloc] initWithFrame: xRect :xImageName];
    [self.view addSubview: _linkGLProgram];
    
    y = y + 24 + space;
    xRect = CGRectMake(x, y, 133, 24);
    xImageName = @"ImgLinkAttributes.png";
    _linkAttributes = [[BlockView alloc] initWithFrame: xRect :xImageName];
    [self.view addSubview: _linkAttributes];
    
    y = y + 24 + space;
    xRect = CGRectMake(x, y, 78, 71);
    xImageName = @"ImgRender.png";
    _render = [[BlockView alloc] initWithFrame: xRect :xImageName];
    [self.view addSubview: _render];
    
    y = y + 71 + space;
    xRect = CGRectMake(x, y, 100, 19);
    xImageName = @"ImgGLClearColor.png";
    _GLClearColor = [[BlockView alloc] initWithFrame: xRect :xImageName];
    [self.view addSubview: _GLClearColor];
    
    y = y + 18 + space;
    xRect = CGRectMake(x, y, 100, 19);
    xImageName = @"ImgGLDepthTest.png";
    _GLDepthTest = [[BlockView alloc] initWithFrame: xRect :xImageName];
    [self.view addSubview: _GLDepthTest];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)showOpenSaveAlert:(BOOL)forSave {
    NSArray *xPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, 
                                                          NSUserDomainMask, YES);
    NSString *xDocumentsDirectory = [xPaths objectAtIndex:0];
    if (forSave) {
        //mostra um alerta normal pedindo apenas o nome do exercício
        UIAlertView *xSaveAlert = [[UIAlertView alloc] initWithTitle:@"Arquivo" 
                                                       message:@"Nome do exercício"
                                                       delegate:self
                                                       cancelButtonTitle:@"Ok"
                                                       otherButtonTitles: nil];
        
        [xSaveAlert setAlertViewStyle:UIAlertViewStylePlainTextInput];
        [xSaveAlert show];
    } else {
        //mostra o alert personalizado que lista os arquivos
        NSArray *xData = [[NSFileManager defaultManager] contentsOfDirectoryAtPath
                                                  :xDocumentsDirectory error: nil];
        BrowsingFileAlertView *xOpenAlert;
        xOpenAlert = [[BrowsingFileAlertView alloc] initWithTitle:xData :@"Exercício" 
                                                    delegate:self 
                                                    cancelButtonTitle:@"Ok" 
                                                    otherButtonTitles:nil];
        [xOpenAlert show];
    }
}

- (IBAction)saveBlocks:(id)sender {
    NSMutableString *xmlSave = [NSMutableString stringWithString:@"<blocks>"];
    for (Block* xBlock in [_controller getBaseBlocks]) {
        [xBlock saveToXML:xmlSave];
    }
    [xmlSave appendString:@"</blocks>"];

    //gravar no disco o xml
    _xmlToSave = xmlSave;
    _forSave = YES;
    [self showOpenSaveAlert:_forSave];
}

- (IBAction)loadBlocks:(id)sender {
    _forSave = NO;
    [self showOpenSaveAlert:_forSave];
}

- (BlockView*)createBlock:(NSString*)aClassName {
    BlockView *xNewBlock = nil;
    if ([aClassName isEqualToString:@"GLContextBlock"]) {
        NSMutableArray* xConnectors = [[NSMutableArray alloc] initWithObjects: [[Connector alloc] init:35 : 31], nil];
        xNewBlock = [self createBaseBlockView:_GLContext.imageName :_GLContext.frame :xConnectors :GLContextBlock.class];
    } else if ([aClassName isEqualToString:@"OpenGLES2Block"]) {
        xNewBlock = [self createBlockView:_OpenGLES2.imageName :_OpenGLES2.frame :CGPointMake(-64, 13) :OpenGLES2Block.class];
    } else if ([aClassName isEqualToString:@"GLProgramBlock"]) {
        NSMutableArray* xConnectors = [[NSMutableArray alloc] initWithObjects: [[Connector alloc] init:45 :26], 
                                       [[Connector alloc] init:45 :83], nil];
        xNewBlock = [self createBaseBlockView:_GLProgram.imageName :_GLProgram.frame :xConnectors :GLProgramBlock.class];
    } else if ([aClassName isEqualToString:@"VertexShaderBlock"]) {
        xNewBlock = [self createBlockView:_vertexShader.imageName :_vertexShader.frame :CGPointMake(-53, 20) :VertexShaderBlock.class];
    } else if ([aClassName isEqualToString:@"FragmentShaderBlock"]) {
        xNewBlock = [self createBlockView:_fragmentShader.imageName :_fragmentShader.frame :CGPointMake(-53, 20) :FragmentShaderBlock.class];
    } else if ([aClassName isEqualToString:@"MainBlock"]) {
        NSMutableArray* xConnectors = [[NSMutableArray alloc] initWithObjects: [[Connector alloc] init:50 :20], 
                                       [[Connector alloc] init:50 :50],
                                       [[Connector alloc] init:70 :80],
                                       [[Connector alloc] init:85 :98],
                                       [[Connector alloc] init:95 :170], nil];
        xNewBlock = [self createBaseBlockView:_main.imageName :_main.frame :xConnectors :MainBlock.class];
    } else if ([aClassName isEqualToString:@"CompileVertexShaderBlock"]) {
        xNewBlock = [self createBlockView:_compileVertexShader.imageName :_compileVertexShader.frame :CGPointMake(-49, 12) :CompileVertexShaderBlock.class];
    } else if ([aClassName isEqualToString:@"CompileFragmentShaderBlock"]) {
        xNewBlock = [self createBlockView:_compileFragmentShader.imageName :_compileFragmentShader.frame :CGPointMake(-49, 12) :CompileFragmentShaderBlock.class];
    } else if ([aClassName isEqualToString:@"LinkAttributesBlock"]) {
        xNewBlock = [self createBlockView:_linkAttributes.imageName :_linkAttributes.frame :CGPointMake(-13, 1) :LinkAttributesBlock.class];
    } else if ([aClassName isEqualToString:@"LinkGLProgramBlock"]) {
        xNewBlock = [self createBlockView:_linkGLProgram.imageName :_linkGLProgram.frame :CGPointMake(-30, 13) :LinkGLProgramBlock.class];
    } else if ([aClassName isEqualToString:@"RenderBlock"]) {
        NSMutableArray* xConnectors = [[NSMutableArray alloc] initWithObjects: [[Connector alloc] init:34 :29], 
                                       [[Connector alloc] init:34 :59], nil];
        xNewBlock = [self createIntermediateBlockView:_render.imageName :_render.frame :xConnectors :CGPointMake(-17, 39) :RenderBlock.class];
    } else if ([aClassName isEqualToString:@"GLClearColorBlock"]) {
        xNewBlock = [self createBlockView:_GLClearColor.imageName :_GLClearColor.frame :CGPointMake(-24, 19) :GLClearColorBlock.class];
    } else if ([aClassName isEqualToString:@"GLDepthTestBlock"]){
        xNewBlock = [self createBlockView:_GLDepthTest.imageName :_GLDepthTest.frame :CGPointMake(-24, 19) :GLDepthTestBlock.class];
    }
    
    if (xNewBlock) {
        [_controller addBlockView:xNewBlock];
        [self.view addSubview:xNewBlock];
    }
    return xNewBlock;
}

- (void) parser:(NSXMLParser *) parser didStartElement:(NSString *) elementName namespaceURI:(NSString *) namespaceURI qualifiedName:(NSString *) qName attributes:(NSDictionary *) attributeDict {
    if ([elementName isEqualToString:@"center"]) {
        _isCenter = YES;
    } else if ([elementName isEqualToString:@"class"]) {
        _isClass = YES;
    } else if ([elementName isEqualToString:@"params"]) {
        _isParams = YES;
    }
}

- (void)parser:(NSXMLParser *) parser foundCharacters:(NSString *)string {
    if (_isCenter) {
        _center = CGPointFromString(string);
        _isCenter = NO;
    } else if (_isClass) {
        BlockView *xBlock = [self createBlock:string];
        [xBlock updateLocationFromCenter:_center.x :_center.y];
        xBlock.center = _center;
        if ([_controller endMoveBlockView:xBlock]) {
            _lastBlockLoaded = xBlock;
        } else {
            _lastBlockLoaded = nil;
        }
        _isClass = NO;
    } else if (_isParams) {
        //alguns blocos possuem parâmetros então a string virá com os valores deles
        if (_lastBlockLoaded && [[_lastBlockLoaded.modelBlock getClassName] isEqualToString:@"GLClearColorBlock"]) {
            NSArray *values = [string componentsSeparatedByString:@","];
            float red = [[values objectAtIndex:0] floatValue];
            float green = [[values objectAtIndex:1] floatValue];
            float blue = [[values objectAtIndex:2] floatValue];
            [(GLClearColorBlock*)_lastBlockLoaded.modelBlock setColor:red :green :blue];
        } else if (_lastBlockLoaded && [[_lastBlockLoaded.modelBlock getClassName] isEqualToString:@"GLDepthTestBlock"]) {
            if ([string isEqualToString:@"yes"]) {
                [(GLDepthTestBlock*)_lastBlockLoaded.modelBlock setEnableDepthTest: YES];
            } else {
                [(GLDepthTestBlock*)_lastBlockLoaded.modelBlock setEnableDepthTest: NO];
            }
        }
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    if ([elementName isEqualToString:@"block"]) {
        _lastBlockLoaded = nil;
    } else if ([elementName isEqualToString:@"params"]) {
        _isParams = NO;
    }
}

- (void)alertView:(UIAlertView *)alertView 
            clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *xFileName = @"blocks";
    NSArray *xPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, 
                                                          NSUserDomainMask, YES);
    NSString *xDirectory = [xPaths objectAtIndex:0];
    if (_forSave) {
        //sabe-se que está salvando, então a classe é UIAlertView mesmo
        if (![[alertView textFieldAtIndex:0].text isEqualToString:@""]) {
            xFileName = [alertView textFieldAtIndex:0].text;
        }
        NSString *xFilePath = [xDirectory stringByAppendingPathComponent:xFileName];
        xFilePath = [xFilePath stringByAppendingString:@".xml"];
        [_xmlToSave writeToFile:xFilePath 
                    atomically:YES 
                    encoding:NSASCIIStringEncoding
                    error:nil];    
    } else {
        //está lendo, então a classe do alertView é a BrowsingFileAlertView
        if (![((BrowsingFileAlertView*)alertView).selectedData isEqualToString:@""])
        {
            [_controller clear];
            xFileName = ((BrowsingFileAlertView*)alertView).selectedData;
        }
        NSString *xFilePath = [xDirectory stringByAppendingPathComponent:xFileName];
        NSData *xmlData = [[NSMutableData alloc] initWithContentsOfFile:xFilePath];
        NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:xmlData];
        [xmlParser setDelegate:self];
        [xmlParser setShouldResolveExternalEntities:YES];
        [xmlParser parse];
    }
}


@end









