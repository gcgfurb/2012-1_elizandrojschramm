//
//  BlockView.h
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 14/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Block.h"

@interface BlockView : UIImageView {
    double _height;
}

- (id)initWithFrame:(CGRect)aFrame:(NSString*)aImageName:(Class)aClassOfBlockModel;
- (id)initWithFrame:(CGRect)aFrame :(NSString *)aImageName;
- (double)getHeight;
- (void)remove;
- (void)updateLocationFromCenter:(double)aX:(double)aY;
- (void)updateLocationFromOrigin:(double)aX:(double)aY;

@property Block* modelBlock;
@property NSString* imageName;


@end
