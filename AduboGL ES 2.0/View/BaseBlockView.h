//
//  BaseBlockView.h
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 18/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "BlockView.h"
#import "BaseBlock.h"

@interface BaseBlockView : BlockView

- (void)addLinkedBlock:(BlockView*)aBlock;
- (void)removeLinkedBlock:(BlockView*)aBlock;
- (NSMutableArray*)getLinkedBlocks;

@end
