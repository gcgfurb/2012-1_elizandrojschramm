//
//  OpenGLView.m
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 26/03/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "OpenGLView.h"
#import "MatrixUtil.h"

@implementation OpenGLView

@synthesize properties;

//record apenas para auxiliar na separação entre posição e cor dos vértices.
typedef struct {
    float Position[3];
    float Color[4];
} Vertex;

typedef enum {
    ATTRIB_VERTEX
} TypeAttributeGLSL;


//era no início quando era 2D
/*const Vertex Vertices[] = {
    {{1, -1, 0}, {1, 0, 0, 1}},
    {{1, 1, 0}, {0, 1, 0, 1}},
    {{-1, 1, 0}, {0, 0, 1, 1}},
    {{-1, -1, 0}, {0, 0, 0, 1}}
};

const GLubyte Indices[] = {
    0, 1, 2,
    2, 3, 0
};*/

const Vertex Vertices[] = {
    {{1, -1, 0}, {1, 0, 0, 1}},
    {{1, 1, 0}, {1, 0, 0, 1}},
    {{-1, 1, 0}, {0, 1, 0, 1}},
    {{-1, -1, 0}, {0, 1, 0, 1}},
    {{1, -1, -1}, {1, 0, 0, 1}},
    {{1, 1, -1}, {1, 0, 0, 1}},
    {{-1, 1, -1}, {0, 1, 0, 1}},
    {{-1, -1, -1}, {0, 1, 0, 1}}
};

const GLubyte Indices[] = {
    // Front
    0, 1, 2,
    2, 3, 0,
    // Back
    4, 6, 5,
    4, 7, 6,
    // Left
    2, 7, 3,
    7, 6, 2,
    // Right
    0, 4, 1,
    4, 1, 5,
    // Top
    6, 2, 1, 
    1, 6, 5,
    // Bottom
    0, 3, 7,
    0, 7, 4    
};
/*
 "When you render geometry with OpenGL, keep in mind that it can’t render squares – it can only render triangles."
 */


/*
 Configura o layer como opaco.
 Segundo raywenderlich se usar não opaco (transparente) afeta a performance.
 */
- (void)setupLayer {
    _eaglLayer = (CAEAGLLayer*) self.layer;
    _eaglLayer.opaque = YES; //por default é transparente
}


/* 
 Para não parecer que estamos olhando dentro do cubo (ou qualquer outro shape): habilitamos o teste de profundidade.
 Assim a OpenGL pode manter o controle da coordenada Z para cada pixel que quer desenhar e desenha o vértice apenas
 se já não houver nada na frente dele.
 */
- (void)setupDepthBuffer {
    glGenRenderbuffers(1, &_depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _depthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, self.frame.size.width, self.frame.size.height);    
}


/*
 Cria um buffer que armazenará a imagem renderizada presenta na tela.
 */
- (void)setupRenderBuffer {
    //cria um novo buffer de renderização e retorna um integer unico que será salvo no _colorRenderBuffer
    glGenRenderbuffers(1, &_colorRenderBuffer); 
    //diz para a OpenGL que quando eu me referir a GL_RENDERBUFFER significa que estou me referindo ao _colotRenderBuffer
    glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);
    //arruma um lugar no context para armazenar este renderBuffer.
    [self.properties.context renderbufferStorage:GL_RENDERBUFFER fromDrawable:_eaglLayer];    
}


/*
 Cria o frame buffer que conterá o render buffer e alguns outros buffers, como depth buffer,
 stencil buffer e accumulation buffer (estes dois últimos não usei aqui.
 */
- (void)setupFrameBuffer {    
    GLuint framebuffer;
    glGenFramebuffers(1, &framebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
    //anexa o buffer render ao frame buffer
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, _colorRenderBuffer);
    //anexa o buffer depth ao frame buffer
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, _depthRenderBuffer);
    //não entendi direito ainda porque tem que fazer isso...
}


/*
 Cria os Vertex Buffers Objects.
 Parece ser o melhor caminho de enviar dados para OpenGL.
 Basicamente porque existem funções da OpenGL que armazenam os dados dos vértices para nós.
 Usamos dois tipos de VBOs:
    - um para manter os dados de cada vértice (Vertices array);
    - outro para manter os índices que maqueiam os triângulos (Indices array).
 */
- (void)setupVBOs {
    
    GLuint vertexBuffer;
    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertices), Vertices, GL_STATIC_DRAW);
    
    GLuint indexBuffer;
    glGenBuffers(1, &indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Indices), Indices, GL_STATIC_DRAW);
    
}

- (void)render:(CADisplayLink*)displayLink {
    //projeção da câmera.
    GLfloat modelView[16], projection[16];
    [MatrixUtil mtxLoadPerspective:projection :90 :988 / 648 :5.0 :10000];
    [MatrixUtil mtxLoadIdentity:modelView];	
    
    _currentRotation += displayLink.duration * 90;
	[MatrixUtil mtxTranslateApply:modelView :sin(CACurrentMediaTime()) :0 :-7];
	[MatrixUtil mtxRotateXApply:modelView :_currentRotation];	
    [MatrixUtil mtxRotateYApply:modelView :_currentRotation];	
    
    if (self.properties.projectionUniform < INT32_MAX) {
        glUniformMatrix4fv(self.properties.projectionUniform, 1, GL_FALSE, projection);
    }
    if (self.properties.modelViewUniform < INT32_MAX) {
        glUniformMatrix4fv(self.properties.modelViewUniform, 1, GL_FALSE, modelView);
    }
    
    //setar a posição do UIView que será usada para renderizar a imagem
    glViewport(0, 0, 988, 648);
    
    //só funcionam porque fizemos isso glGetAttribLocation no compileShaders.
    if (self.properties.positionSlot < INT32_MAX) {
        glVertexAttribPointer(self.properties.positionSlot, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0); //o útltimo parâmetro é o offset
    }
    if (self.properties.colorSlot < INT32_MAX) {
        glVertexAttribPointer(self.properties.colorSlot, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) (sizeof(float) *3));
    }
    
    //desenhar os elementos
    if (self.properties.programHandle < INT32_MAX && self.properties.positionSlot < INT32_MAX && self.properties.colorSlot < INT32_MAX) {
        //pág. 134 do OpenGL ES 2.0 Programming guide
        glDrawElements(GL_TRIANGLES, sizeof(Indices)/sizeof(Indices[0]), GL_UNSIGNED_BYTE, 0);
    }
    
    [self.properties.context presentRenderbuffer:GL_RENDERBUFFER];
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder: aDecoder];
    if (self) {        
        [self setupLayer];        
    }
    return self;
}

- (void)start {
    [self setupDepthBuffer];
    [self setupRenderBuffer];        
    [self setupFrameBuffer];    
    [self setupVBOs];
}

+ (Class)layerClass { //sobreescreve o do pai para retornar a classe da OpenGL como default
    return [CAEAGLLayer class];
}

@end
