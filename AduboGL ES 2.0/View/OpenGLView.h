//
//  OpenGLView.h
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 26/03/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#include <OpenGLES/ES2/gl.h>
#include <OpenGLES/ES2/glext.h>
#import "OpenGLProperties.h"

@interface OpenGLView : UIView {
    CAEAGLLayer* _eaglLayer;
    EAGLContext* _context;
    GLuint _colorRenderBuffer;
    float _currentRotation;
    GLuint _depthRenderBuffer;
}

- (void)start;
- (void)render:(CADisplayLink*)displayLink;

@property OpenGLProperties* properties;

@end
