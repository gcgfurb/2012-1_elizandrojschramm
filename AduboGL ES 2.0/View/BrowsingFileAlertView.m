//
//  BrowsingFileAlertView.m
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 30/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "BrowsingFileAlertView.h"

@implementation BrowsingFileAlertView {
    NSString *_selectedData;
}

@synthesize tableData = _tableData;
@synthesize data = _data;
@synthesize selectedData = _selectedData;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor colorWithWhite:0.90 alpha:1.0];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _selectedData = @"";
    }
    return self;
}

- (id)initWithTitle:(NSArray*)array:(NSString *)title delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles {
    self = [super initWithTitle:title message:@"" delegate:delegate cancelButtonTitle:cancelButtonTitle otherButtonTitles:otherButtonTitles, nil];
    if (self) {
        self.data = [array mutableCopy];
    }
    return self;
}

- (void)setFrame:(CGRect)rect {
    [super setFrame:CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, 310)];
}

- (void)layoutSubviews {
    CGFloat buttonTop = 0, width = -2;
    for (UIView *view in self.subviews) {
        if ([[[view class] description] isEqualToString:@"UIAlertButton"]) {
            view.frame = CGRectMake(view.frame.origin.x, self.bounds.size.height - view.frame.size.height - 15, view.frame.size.width, view.frame.size.height);
            buttonTop = view.frame.origin.y;
            width += view.frame.size.width;
        }
    }
    
    buttonTop -= 207;
    
    UIView* container = [[UIView alloc] initWithFrame:CGRectMake(12, buttonTop, width, 200)];
    container.backgroundColor = [UIColor whiteColor];
    container.clipsToBounds = YES;
    [self addSubview:container];
    
    _tableView.frame = container.bounds;
    [container addSubview:_tableView];
    
    UIGraphicsBeginImageContext(container.frame.size);
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), 2.0);
    CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.9, 0.9, 0.9, 1.0);
    CGContextSetShadow(UIGraphicsGetCurrentContext(), CGSizeMake(0, -3), 3.0);
    CGContextStrokeRect(UIGraphicsGetCurrentContext(), CGContextGetClipBoundingBox(UIGraphicsGetCurrentContext()));
    UIImageView *imageView = [[UIImageView alloc] initWithImage:UIGraphicsGetImageFromCurrentImageContext()];
    [container addSubview:imageView];
    UIGraphicsEndImageContext();
}

- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section {
    return [_data count];	
}

- (NSString*)tableView:(UITableView*)tableView titleForHeaderInSection:(NSInteger)section {	
    return nil;
}

- (UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath {
    static NSString *CellIdentifier = @"Cell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

    switch (indexPath.section) {
        case 0:
            cell.textLabel.text = [_data objectAtIndex:indexPath.row];	
            break;
        default:
            break;
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    _selectedData = [_data objectAtIndex:indexPath.row];
}

#pragma mark
#pragma mark Property Methods

- (void)setMessage:(NSString *)message {
    return;
}

- (NSString*)message {
    return nil;
}

@end















