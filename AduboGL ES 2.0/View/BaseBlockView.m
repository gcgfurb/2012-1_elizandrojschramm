//
//  BaseBlockView.m
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 18/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "BaseBlockView.h"

@implementation BaseBlockView {
    NSMutableArray* _linkedBlocks;
}

- (id)initWithFrame:(CGRect)aFrame:(NSString*)aImageName:(Class)aClassOfBlockModel {
    self = [super initWithFrame:aFrame :aImageName :aClassOfBlockModel];
    if (self) {
        _linkedBlocks = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)addLinkedBlock:(BlockView*)aBlock {
    [_linkedBlocks addObject:aBlock];
}

- (void)removeLinkedBlock:(BlockView*)aBlock {
    [_linkedBlocks removeObject:aBlock];
}

- (NSMutableArray*)getLinkedBlocks {
    return _linkedBlocks;
}


- (double)getHeight {
    return _height - 20;
    //Este 20 é a parte de "engate" que os blocos base têm.
}

- (BlockView*)getLinkedBlockView:(Block*)aBlock {
    for (BlockView *xBlock in _linkedBlocks) {
        if (xBlock.modelBlock == aBlock) {
            return xBlock;
        }
    }
    return nil;
}

- (void)updateLocationFromCenter:(double)aX:(double)aY {
    CGFloat x = aX - self.modelBlock.center.x;
    CGFloat y = aY - self.modelBlock.center.y;
    CGPoint xDistance = CGPointMake(x, y);
    [super updateLocationFromCenter:aX :aY];
    //atualizar os filhos também
    BaseBlock *xBaseBlock = (BaseBlock*)self.modelBlock;
    BlockView *xBlockView = nil;
    double xBx, xBy;
    for (Block *xBlock in [xBaseBlock children]) {
        xBlockView = [self getLinkedBlockView: xBlock];
        if (xBlockView) {
            xBx = xBlock.center.x + xDistance.x;
            xBy = xBlock.center.y + xDistance.y;
            if ([xBlockView isKindOfClass: BaseBlockView.class]) {
                //e quando um filho é pai também
                [xBlockView updateLocationFromCenter:xBx :xBy];
                xBlockView.center = xBlock.center;
            } else {
                [xBlock updateLocationFromCenter:xBx :xBy];
                xBlockView.center = xBlock.center;
            }
        }
    }
    //atualizar os irmãos também
    for (Block *xBlock in [xBaseBlock siblings]) {
        xBlockView = [self getLinkedBlockView: xBlock];
        if (xBlockView) {
            xBx = xBlock.center.x + xDistance.x;
            xBy = xBlock.center.y + xDistance.y;
            if ([xBlockView isKindOfClass: BaseBlockView.class]) {
                //e quando um irmão é pai também
                [xBlockView updateLocationFromCenter:xBx :xBy];
                xBlockView.center = xBlock.center;
            } else {
                [xBlock updateLocationFromCenter:xBx :xBy];
                xBlockView.center = xBlock.center;
            }
        }
    }
}

- (void)updateLocationFromOrigin:(double)aX:(double)aY {
    CGPoint xDistance = CGPointMake(aX - self.modelBlock.location.x, aY - self.modelBlock.location.y);
    [super updateLocationFromOrigin:aX :aY];

    //atualizar os filhos também
    BaseBlock *xBaseBlock = (BaseBlock*)self.modelBlock;
    BlockView *xBlockView = nil;
    for (Block *xBlock in [xBaseBlock children]) {
        xBlockView = [self getLinkedBlockView:xBlock];
        if (xBlockView) {
            if ([xBlockView isKindOfClass: BaseBlockView.class]) {
                //e quando um filho é pai também
                [xBlockView updateLocationFromOrigin:xBlock.location.x + xDistance.x :xBlock.location.y + xDistance.y];
                xBlockView.center = xBlock.center;
            } else {
                [xBlock updateLocationFromOrigin:xBlock.location.x + xDistance.x :xBlock.location.y + xDistance.y];
                xBlockView.center = xBlock.center;
            }
        }
    }
}


@end
