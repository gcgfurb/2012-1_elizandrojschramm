//
//  BlockView.m
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 14/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "BlockView.h"
#import "BaseBlock.h"

@implementation BlockView {
    Block* _modelBlock;
}

@synthesize modelBlock = _modelBlock;
@synthesize imageName;

/**
 * Este construtor é chamado quando for um bloco que será manipulado pelo Aluno.
 **/
- (id)initWithFrame:(CGRect)aFrame:(NSString*)aImageName:(Class)aClassOfBlockModel {
    self = [self initWithFrame:aFrame :aImageName];
    if (self) {
        CGPoint xLocation = CGPointMake(aFrame.origin.x, aFrame.origin.y);
        CGPoint xCenter = CGPointMake(self.center.x, self.center.y);
        _modelBlock = [(Block*)[aClassOfBlockModel alloc] init:xLocation :xCenter];
        _height = self.image.size.height; //fixa a altura da imagem que foi criado, pois quando seleciona troca de imagem
    }
    return self;
}

/**
 * Este contrutor é chamado quando for um bloco que fica apenas de índice na caixa de blocos. Ele não será manupulado
 * pelo Aluno. O Aluno clica sobre ele e a aplicação cria um bloco "cópia" para ser manupilado.
 **/
- (id)initWithFrame:(CGRect)aFrame :(NSString *)aImageName {
    self = [super initWithFrame:aFrame];
    if (self) {
        self.image = [UIImage imageNamed:aImageName];
        self.imageName = aImageName;
        self.userInteractionEnabled = YES;
    }
    return self;
}

- (double)getHeight {
    return _height;
}

- (void)remove {
    [_modelBlock remove];
    [self removeFromSuperview];
}

- (void)updateLocationFromCenter:(double)aX:(double)aY {
    [_modelBlock updateLocationFromCenter:aX :aY];
}

- (void)updateLocationFromOrigin:(double)aX:(double)aY {
    [_modelBlock updateLocationFromOrigin:aX :aY];
}

@end
