//
//  BrowsingFileAlertView.h
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 30/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrowsingFileAlertView : UIAlertView <UITableViewDelegate, UITableViewDataSource> {
    id _tableData;
    NSMutableArray *_data;
@private
    UITableView *_tableView;
}

- (id)initWithTitle:(NSArray*)array:(NSString *)title delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles;

@property id tableData;
@property (nonatomic, retain) NSMutableArray *data;
@property NSString *selectedData;

@end
