//
//  GLProgramBlock.m
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 15/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "GLProgramBlock.h"
#import "OpenGLProperties.h"

@implementation GLProgramBlock

- (void)createProgram:(OpenGLProperties*)aGLProp {
    aGLProp.programHandle = glCreateProgram();
    for (Block *xChild in _children) {
        [xChild performSelector: xChild.operation withObject:aGLProp]; 
    }
}

- (id)init:(CGPoint)aLocation:(CGPoint)aCenter {
    self = [super init :aLocation :aCenter];
    if (self) {
        _operation = @selector(createProgram:);
        _sequence = 2; //Tem que ser maior do que o outro bloco filho de GLContext
    }
    return self;
}

- (BOOL)canReceiveThisAsChild:(Block*)aChild {
    NSString* xName = aChild.getClassName;
    return xName == @"VertexShaderBlock" || xName == @"FragmentShaderBlock";
}

- (BOOL)canReceiveThisAsSibling:(Block*)aSibling {
    return aSibling.getClassName == @"MainBlock";
}

- (NSString*)getClassName {
    return @"GLProgramBlock";
}

- (NSString*)getReadableCode {
    return @"glCreateProgram();";
}

- (NSString*)getName {
    return @"GLProgram";
}

@end
