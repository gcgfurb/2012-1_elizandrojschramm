//
//  GLDepthTestBlock.m
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 15/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "GLDepthTestBlock.h"

@implementation GLDepthTestBlock {
    BOOL _enableDT;
}

- (void)enableDepth {
    if (_enableDT) {
        glEnable(GL_DEPTH_TEST);
    } else {
        glDisable(GL_DEPTH_TEST);
    }
}

- (id)init:(CGPoint)aLocation:(CGPoint)aCenter {
    self = [super init :aLocation :aCenter];
    if (self) {
        _operation = @selector(enableDepth);
        _sequence = 2;
        _enableDT = YES;
    }
    return self;
}

- (NSString*)getClassName {
    return @"GLDepthTestBlock";
}

- (void)setEnableDepthTest:(BOOL)aValue {
    _enableDT = aValue;
}

- (BOOL)getEnableDepthTest {
    return _enableDT;
}

- (NSString*)getReadableCode {
    if (_enableDT) {
        return @"glEnable(GL_DEPTH_TEST);";
    } else {
        return @"glDisable(GL_DEPTH_TEST);";
    }
}

- (NSString*)getName {
    return @"GLDepthTest";
}

- (void)saveToXML:(NSMutableString*)aXMLElement {
    NSString *xEnabled = @"no";
    if (_enableDT) {
        xEnabled = @"yes";
    }
    NSString *xValues = [NSString stringWithFormat:@"<block><center>{%.2f,%.2f}</center><class>%@</class><params>"
                         "%@</params></block>", self.center.x, self.center.y, [self getClassName], xEnabled];
    [aXMLElement appendString:xValues];
}

@end











