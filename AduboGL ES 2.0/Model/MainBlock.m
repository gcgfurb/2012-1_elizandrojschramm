//
//  MainBlock.m
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 14/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "MainBlock.h"
#import "OpenGLProperties.h"

@implementation MainBlock

- (void)main:(OpenGLProperties*)aGLProp {
    for (Block *xChild in [self getOrderedChildren]) {
        if (xChild.getClassName != @"RenderBlock") { //o Render é chamado apenas por demanda pelo Controller da OpenGL
            [xChild performSelector: xChild.operation withObject:aGLProp]; 
        }
        
    }
}

- (id)init:(CGPoint)aLocation:(CGPoint)aCenter {
    self = [super init :aLocation :aCenter];
    if (self) {
        _operation = @selector(main:);
    }
    return self;
}

- (BOOL)canReceiveThisAsChild:(Block*)aChild {
    NSString* xName = aChild.getClassName;
    return xName == @"CompileVertexShaderBlock" || xName == @"CompileFragmentShaderBlock" || xName == @"LinkAttributesBlock" || xName == @"LinkGLProgramBlock" || xName == @"RenderBlock";
}

- (NSString*)getClassName {
    return @"MainBlock";
}

- (NSString*)getName {
    return @"Main";
}

@end
