//
//  RenderBlock.m
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 15/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "RenderBlock.h"

@implementation RenderBlock

- (void)render {
    for (Block *xChild in [self getOrderedChildren]) {
        [xChild performSelector: xChild.operation]; 
    }
}

- (void)updateLocationFromCenter:(double)aX:(double)aY {
    [super updateLocationFromCenter:aX :aY];
}

- (void)updateLocationFromOrigin:(double)aX:(double)aY {
    [super updateLocationFromOrigin:aX :aY];
}

- (id)init:(CGPoint)aLocation:(CGPoint)aCenter {
    self = [super init :aLocation :aCenter];
    if (self) {
        _operation = @selector(render);
    }
    return self;
}

- (BOOL)canReceiveThisAsChild:(Block*)aChild {
    NSString* xName = aChild.getClassName;
    return xName == @"GLClearColorBlock" || xName == @"GLDepthTestBlock";
}

- (NSString*)getClassName {
    return @"RenderBlock";
}

- (NSString*)getName {
    return @"Render";
}

@end
