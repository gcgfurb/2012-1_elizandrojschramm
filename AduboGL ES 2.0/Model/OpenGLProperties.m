//
//  Code.m
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 24/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "OpenGLProperties.h"

@implementation OpenGLProperties

@synthesize colorSlot;
@synthesize context;
@synthesize fragmentShaderSrt;
@synthesize modelViewUniform;
@synthesize positionSlot;
@synthesize programHandle;
@synthesize projectionUniform;
@synthesize vertexShaderStr;

- (id)init {
    self = [super init];
    if (self) {
        self.programHandle = INT32_MAX;
        self.positionSlot = INT32_MAX;
        self.colorSlot = INT32_MAX;
        self.projectionUniform = INT32_MAX;
        self.modelViewUniform = INT32_MAX;
        self.vertexShaderStr = @"";
        self.fragmentShaderSrt = @"";
    }
    return self;
}

@end
