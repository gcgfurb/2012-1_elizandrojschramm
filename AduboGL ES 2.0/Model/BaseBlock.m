//
//  BaseBlock.m
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 14/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "BaseBlock.h"

@class BaseBlock;

@implementation BaseBlock {
    NSMutableArray* _clamps;
}

@synthesize clamps = _clamps;
@synthesize children = _children;
@synthesize siblings = _siblings;

/**
 * Inicializa o objeto.
 * @param aLocation localização que será as coordenadas x,y do bloco.
 * @param aCenter será o centro do bloco.
 * @return o id referência do objeto.
 */
- (id)init:(CGPoint)aLocation:(CGPoint)aCenter {
    self = [super init: aLocation : aCenter];
    if (self) {
        _children = [[NSMutableArray alloc] init];
        _siblings = [[NSMutableArray alloc] init];
        _clamps = [[NSMutableArray alloc] init];
    }
    return self;
}

/**
 * Retorna a lista de filhos ordenada pela sequência de cada bloco.
 */
- (NSArray*)getOrderedChildren {
    return [_children sortedArrayUsingComparator:^(Block *aBlock1, Block *aBlock2) {
        if (aBlock1.sequence > aBlock2.sequence) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        if (aBlock1.sequence < aBlock2.sequence) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }];

}

/**
 * Valida se pode receber um bloco como filho.
 * Cada classe herdeira sobrescreve este método.
 * @param aChild bloco filho.
 * @return se pode ou não receber o bloco como filho.
 */
- (BOOL)canReceiveThisAsChild:(Block*)aChild {
    return NO; //cada classe filha implementa
}

/**
 * Adiciona um bloco filho.
 **/
- (void)addChild:(Block*)aChild {
    [_children addObject: aChild];
}

/**
 * Este método remove o bloco filho bem como a conexão que
 * estava ligando os dois.
 * @param aChild bloco filho a ser removido da lista de filhos.
 * @param aConnector engate que encaixa o bloco filho neste bloco.
 **/
- (void)removeChild:(Block*)aChild:(Connector*)aConnector {
    for (Connector *xConnector in _clamps) {
        if (xConnector != aConnector) {
            if (xConnector.used && xConnector.block == aChild) {
                xConnector.block = nil;
                xConnector.used = NO;
                break;
            }
        }
    }
    [_children removeObject: aChild];
}

/**
 * Adiciona um engate na lista de engates.
 **/
- (void)addClamp:(Connector*)aClamp {
    [_clamps addObject:aClamp];
}

/**
 * Retorna um engate pelo índice.
 **/
- (Connector*)getClamp:(int)aIndex {
    return [_clamps objectAtIndex:aIndex];
} 

/**
 * Este método retorna o ponto de conexão na posição i passada
 * como parâmetro. Ele é atualizado com base
 * na localização atual do bloco.
 **/
- (Connector*)getRelativeClamp:(int)aIndex {
    Connector *xTemp = [self getClamp:aIndex];
    //atualizar o x e y da conexão
    Connector *xResult = [[Connector alloc]init: _location.x + xTemp.x : _location.y + xTemp.y];
    xResult.used = xTemp.used; //new 14/05
    xResult.block = xTemp.block; //new 14/04
    return xResult;
}

/**
 * Este método retorna o ponto de conexão próximo a X e Y
 * passados como parâmetro. Ele é atualizado com base
 * na localização atual do bloco.
 **/
- (Connector*)getClampByXY:(double)aX:(double)aY {
    CGPoint xConnPoint, xPoint;
    //cria um ponto com X e Y recebidos para comparação
    xPoint = CGPointMake(aX, aY);
    for (int i = 0; i < [_clamps count]; i++) {
        Connector *xConnector = [self getClamp: i];
        xConnPoint = CGPointMake(xConnector.x + _location.x, xConnector.y + _location.y);
        //se o X e Y desta conexão é igual ao X e Y recebidos, então é esta a conexão esperada
        if (CGPointEqualToPoint(xPoint, xConnPoint)) {
            return xConnector;
        }
    }
    return nil;
}

/**
 * Adiciona um bloco como irmão.
 **/
- (void)addSibling:(Block*)aSibling {
    [_siblings addObject:aSibling];
}

/**
 * Remove um bloco irmão.
 **/
- (void)removeSibling:(Block*)aSibling {
    [_siblings removeObject:aSibling];
}

/**
 * Retorna se um bloco pode ser conectado como irmão.
 */
- (BOOL)canReceiveThisAsSibling:(Block*)aSibling {
    return NO; //cada classe filha implementa
}

/**
 * Percorre a lista de blocos filhos chamando o saveToXML de cada bloco concatenando no XML final.
 * @param aXMLElement XML que serão escritas as propriedades dos blocos. 
 **/
- (void)saveToXML:(NSMutableString*)aXMLElement {
    
    NSString *xValues = [NSString stringWithFormat:@"<baseblock><center>{%.2f,%.2f}</center><class>%@</class>", self.center.x, self.center.y, [self getClassName]];
    [aXMLElement appendString:xValues];
    if ([_children count] > 0) {
        [aXMLElement appendString:@"<children>"];
    }
    for (Block *xChild in _children) {
        [xChild saveToXML:aXMLElement];
    }
    if ([_children count] > 0) {
        [aXMLElement appendString:@"</children>"];
    }
    [aXMLElement appendString:@"</baseblock>"];
}

@end







