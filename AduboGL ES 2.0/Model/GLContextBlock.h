//
//  GLContextBlcok.h
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 14/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "BaseBlock.h"

@interface GLContextBlock : BaseBlock

@property (readwrite) EAGLRenderingAPI EAGLApi;

@end
