//
//  Connector.m
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 14/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "Connector.h"

@implementation Connector

@synthesize x, y, used, block;

- (id)init:(double)aX:(double)aY {
    self = [super init];
    if (self) {
        x = aX;
        y = aY;
        used = NO;
        block = nil;
    }
    return self; 
}

@end
