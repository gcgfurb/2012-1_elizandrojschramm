//
//  LinkGLProgram.m
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 15/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "LinkGLProgramBlock.h"
#import "OpenGLProperties.h"

@implementation LinkGLProgramBlock

- (void)linkGLProgram:(OpenGLProperties*)aGLProp {
    if (aGLProp.programHandle < INT32_MAX) {
        glLinkProgram(aGLProp.programHandle);
        //verifica se ocorreu tudo certo
        GLint linkSuccess;
        glGetProgramiv(aGLProp.programHandle, GL_LINK_STATUS, &linkSuccess);
        if (linkSuccess == GL_FALSE) {
            GLchar messages[256];
            glGetProgramInfoLog(aGLProp.programHandle, sizeof(messages), 0, &messages[0]);
            NSString *messageString = [NSString stringWithUTF8String:messages];
            NSLog(@"%@", messageString);
        }
        //diz para a OpenGL que é este o programa shader atual dela
        glUseProgram(aGLProp.programHandle);
    }
}

- (id)init:(CGPoint)aLocation:(CGPoint)aCenter {
    self = [super init :aLocation :aCenter];
    if (self) {
        _operation = @selector(linkGLProgram:);
        _sequence = 1; //tem que ser chamado antes do LinkAttributesBlock
    }
    return self;
}

- (NSString*)getClassName {
    return @"LinkGLProgramBlock";
}

- (NSString*)getReadableCode {
    return @"glLinkProgram(programHandle); \n"
            "GLint linkSuccess; \n"
            "glGetProgramiv(programHandle, GL_LINK_STATUS, &linkSuccess); \n"
            "if (linkSuccess == GL_FALSE) { \n"
            "    GLchar messages[256]; \n"
            "    glGetProgramInfoLog(programHandle, sizeof(messages), 0, &messages[0]); \n"
            "    NSString *messageString = [NSString stringWithUTF8String:messages]; \n"
            "    NSLog(@\"%@\", messageString); \n"
            "} \n"
            "glUseProgram(programHandle);";
}

- (NSString*)getName {
    return @"Vincular GLProgram";
}

@end
