//
//  OpenGLES2Block.m
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 14/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "OpenGLES2Block.h"
#import "GLContextBlock.h"

@implementation OpenGLES2Block

- (void)createRenderingAPI:(GLContextBlock*)glContext {
    glContext.EAGLApi = kEAGLRenderingAPIOpenGLES2; //precisa para informar a versão na criação do contexto
}

- (id)init:(CGPoint)aLocation:(CGPoint)aCenter {
    self = [super init :aLocation :aCenter];
    if (self) {
        _operation = @selector(createRenderingAPI:);
        _sequence = 1;
    }
    return self;
}

- (NSString*)getClassName {
    return @"OpenGLES2Block";
}

- (NSString*)getReadableCode {
    return @"EAGLApi = kEAGLRenderingAPIOpenGLES2;";
}

- (NSString*)getName {
    return @"OpenGL ES 2.0";
}

@end
