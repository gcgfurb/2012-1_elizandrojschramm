//
//  CompileVertexShaderBlock.m
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 14/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "CompileVertexShaderBlock.h"
#import "OpenGLProperties.h"

@implementation CompileVertexShaderBlock

- (void)compileVertexShader:(OpenGLProperties*)aGLProp {
    if (aGLProp.programHandle < INT32_MAX) {
        if (aGLProp.vertexShaderStr != @"") {
            GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);    
            //passa para a OpenGL o código do shader convertendo de NSString para C-String
            const char * shaderStringUTF8;
            int shaderStringLength;
            shaderStringUTF8 = [aGLProp.vertexShaderStr UTF8String];    
            shaderStringLength = [aGLProp.vertexShaderStr length];
            glShaderSource(vertexShader, 1, &shaderStringUTF8, &shaderStringLength);
            //compila o shader em tempo de execução
            glCompileShader(vertexShader);
            //uma boa prática para ver se ocorreu algum erro na compilação
            GLint compileSuccess;
            glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &compileSuccess);
            if (compileSuccess == GL_FALSE) {
                GLchar messages[256];
                glGetShaderInfoLog(vertexShader, sizeof(messages), 0, &messages[0]);
                NSString *messageString = [NSString stringWithUTF8String:messages];
                NSLog(@"%@", messageString);
            }
            glAttachShader(aGLProp.programHandle, vertexShader);
        }
    }
}

- (id)init:(CGPoint)aLocation:(CGPoint)aCenter {
    self = [super init :aLocation :aCenter];
    if (self) {
        _operation = @selector(compileVertexShader:);
    }
    return self;
}

- (NSString*)getClassName {
    return @"CompileVertexShaderBlock";
}

- (NSString*)getReadableCode {
    return @"GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER); \n"
            "const char * shaderStringUTF8; \n"
            "int shaderStringLength; \n"
            "shaderStringUTF8 = [vertexStr UTF8String]; \n"
            "shaderStringLength = [vertexStr length]; \n"
            "glShaderSource(vertexShader, 1, &shaderStringUTF8, &shaderStringLength); \n"
            "glCompileShader(vertexShaderShader); \n"
            "GLint compileSuccess; \n"
            "glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &compileSuccess); \n"
            "if (compileSuccess == GL_FALSE) { \n"
            "      GLchar messages[256]; \n"
            "      glGetShaderInfoLog(vertexShader, sizeof(messages), 0, &messages[0]); \n"
            "      NSString *messageString = [NSString stringWithUTF8String:messages]; \n"
            "      NSLog(@\"%@\", messageString); \n"
            "} \n"
            "glAttachShader(programHandle, vertexShader);";
}

- (NSString*)getName {
    return @"Compilar Vertex Shader";
}

@end
