//
//  Block.h
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 14/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Connector.h"

@interface Block : NSObject {
    CGPoint _location;
    SEL _operation;
    int _sequence;
}

- (id)init:(CGPoint)aLocation:(CGPoint)aCenter;
- (void)updateCramp;
- (void)updateLocationFromCenter:(double)aX:(double)aY;
- (void)updateLocationFromOrigin:(double)aX:(double)aY;
- (void)remove;
- (NSString*)getClassName;
- (BOOL)hasBaseBlock;
- (NSString*)getReadableCode;
- (NSString*)getName;

- (void)saveToXML:(NSMutableString*)aXMLElement;

/**
 * Cada bloco possui uma operação ligada ao atributo _operation da classe Block,
 * que é um selector, mais conhecido como ponteiro para método em outras linguagens.
 * Nesta operação o bloco chama as funções da biblioteca OpenGL ES relacionadas a ele.
 * A operação de cada bloco será executada quando o Aluno optar por visualizar o resultado,
 * já as funções que cada bloco chama na operação são exibidas quando o Aluno opta por visualizar código.
 * Esta propriedade acessa o atributo _operation.
 **/
@property SEL operation;
/**
 * Distância da posição até o gancho do bloco.
 **/
@property CGPoint distanceCramp;
/**
 * Bloco base.
 **/
@property Block* baseBlock;
/**
 * Gancho que se conectará ao engate do bloco base.
 **/
@property Connector* cramp;
/**
 * Centro do bloco.
 **/
@property (readonly) CGPoint center;
/**
 * Posição do bloco.
 **/
@property (readonly) CGPoint location;
/**
 * Nível de importância do bloco perante os demais blocos de um bloco base.
 **/
@property (readonly) int sequence;

@end
