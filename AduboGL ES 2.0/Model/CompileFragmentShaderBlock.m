//
//  CompileFragmentShaderBlock.m
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 14/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "CompileFragmentShaderBlock.h"
#import "OpenGLProperties.h"

@implementation CompileFragmentShaderBlock

- (void)compileFragmentShader:(OpenGLProperties*)aGLProp {
    if (aGLProp.programHandle < INT32_MAX) {
        if (aGLProp.fragmentShaderSrt != @"") {
            GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);    
            //passa para a OpenGL o código do shader convertendo de NSString para C-String
            const char * shaderStringUTF8;
            int shaderStringLength;
            shaderStringUTF8 = [aGLProp.fragmentShaderSrt UTF8String];    
            shaderStringLength = [aGLProp.fragmentShaderSrt length];
            glShaderSource(fragmentShader, 1, &shaderStringUTF8, &shaderStringLength);
            //compila o shader em tempo de execução
            glCompileShader(fragmentShader);
            //uma boa prática para ver se ocorreu algum erro na compilação
            GLint compileSuccess;
            glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &compileSuccess);
            if (compileSuccess == GL_FALSE) {
                GLchar messages[256];
                glGetShaderInfoLog(fragmentShader, sizeof(messages), 0, &messages[0]);
                NSString *messageString = [NSString stringWithUTF8String:messages];
                NSLog(@"%@", messageString);
            }
            glAttachShader(aGLProp.programHandle, fragmentShader);
        }
    }
}

- (id)init:(CGPoint)aLocation:(CGPoint)aCenter {
    self = [super init :aLocation :aCenter];
    if (self) {
        _operation = @selector(compileFragmentShader:);
    }
    return self;
}

- (NSString*)getClassName {
    return @"CompileFragmentShaderBlock";
}

- (NSString*)getReadableCode {
    return @"GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER); \n"
            "const char * shaderStringUTF8; \n"
            "int shaderStringLength; \n"
            "shaderStringUTF8 = [fragmentStr UTF8String]; \n"
            "shaderStringLength = [fragmentStr length]; \n"
            "glShaderSource(fragmentShader, 1, &shaderStringUTF8, &shaderStringLength); \n"
            "glCompileShader(fragmentShader); \n"
            "GLint compileSuccess; \n"
            "glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &compileSuccess); \n"
            "if (compileSuccess == GL_FALSE) { \n"
            "      GLchar messages[256]; \n"
            "      glGetShaderInfoLog(fragmentShader, sizeof(messages), 0, &messages[0]); \n"
            "      NSString *messageString = [NSString stringWithUTF8String:messages]; \n"
            "      NSLog(@\"%@\", messageString); \n"
            "} \n"
            "glAttachShader(programHandle, fragmentShader);";
}

- (NSString*)getName {
    return @"Compilar Fragment Shader";
}

@end
