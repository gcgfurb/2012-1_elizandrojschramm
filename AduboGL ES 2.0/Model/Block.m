//
//  Block.m
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 14/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "Block.h"
#import "Connector.h"
#import "BaseBlock.h"

@class Block;

@protocol BlockProtocol <NSObject>
@end

@implementation Block {
    Connector* _cramp;
    CGPoint _center;
    CGPoint distanceFromCenter;
    BaseBlock* _baseBlock;
}

@synthesize operation = _operation;
@synthesize distanceCramp;
@synthesize baseBlock = _baseBlock;
@synthesize cramp = _cramp;
@synthesize center = _center;
@synthesize location = _location;
@synthesize sequence = _sequence;

/**
 * Inicializa o objeto.
 * @param aLocation localização que será as coordenadas x,y do bloco.
 * @param aCenter será o centro do bloco.
 * @return o id referência do objeto.
 */
- (id)init:(CGPoint)aLocation:(CGPoint)aCenter {
    self = [super init];
    if (self) {
        _location = aLocation;
        _center = aCenter;
        distanceFromCenter = CGPointMake(aCenter.x - aLocation.x, aCenter.y - aLocation.y);
        distanceCramp = CGPointMake(0, 0); //Cada bloco deve informar a distancida do seu ponto de engate.
        _baseBlock = nil;
        _sequence = 0; //Por padrão é 0, mas cada bloco que tenha irmãos precisa informar se isso importar na ordem de chamada da operation.
    }
    return self;
}

/**
 * Quando o bloco é movido ele precisa atualizar o ponto de 
 * conexão. O ponto de conexão é um ponto simples com uma certa
 * distância entre o x e y (location) do bloco.
 **/
- (void)updateCramp {
    double x = _location.x + distanceCramp.x;
    double y = _location.y + distanceCramp.y;
    if (_cramp) { //apenas atualiza
        _cramp.x = x;
        _cramp.y = y;
    } else { //a primeira vez cria o ponto
        _cramp = [[Connector alloc] init: x : y];
    }
}

/**
 * Quando o bloco é movido na camada de visual, ele é movido
 * apenas pelo centro da imagem, mas precisa atualizar o x e y
 * do bloco modelo para saber onde está.
 * Este método é chamado pelo controle recebendo o x e y 
 * do centro da imagem. Ele atualiza a localização x e y bem
 * como o ponto de conexão.
 **/
- (void)updateLocationFromCenter:(double)aX:(double)aY {
    double x = aX - distanceFromCenter.x;
    double y = aY - distanceFromCenter.y;
    _location = CGPointMake(x, y);
    _center = CGPointMake(aX, aY);
    [self updateCramp];
}

/**
 * Já neste método a localização do bloco é atualizada pela origem, ou seja,
 * pelo X e Y iniciais e não do centro da figura.
 **/
- (void)updateLocationFromOrigin:(double)aX:(double)aY {
    _location = CGPointMake(aX, aY);
    double x = aX + distanceFromCenter.x;
    double y = aY + distanceFromCenter.y;
    _center = CGPointMake(x, y);
    [self updateCramp];
}

/**
 * Remove o bloco do bloco base que ele está encaixado.
 **/
- (void)remove {
    if ([self hasBaseBlock]) {
        [_baseBlock removeChild:self :nil];
    }
}

/**
 * Retorna o nome da classe.
 **/
- (NSString*)getClassName {
    return @"Block";
}

/**
 * Retorna se o bloco possui um bloco base.
 **/
- (BOOL)hasBaseBlock{
    return _baseBlock != nil;
}

/**
 * Retorna o conjunto de funções OpenGL ES que o bloco invoca.
 **/
- (NSString*)getReadableCode {
    return @""; //cada classe implementa o seu
}

/**
 * Retorna o nome do bloco.
 **/
- (NSString*)getName {
    return @"Block";
}

/**
 * Escreve no XML as coordenadas do centro do bloco e o nome da classe.
 **/
- (void)saveToXML:(NSMutableString*)aXMLElement {
    NSString *xValues = [NSString stringWithFormat:@"<block><center>{%.2f,%.2f}</center><class>%@</class></block>", _center.x, _center.y, [self getClassName]];
    [aXMLElement appendString:xValues];
}


@end







