//
//  GLContextBlcok.m
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 14/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "GLContextBlock.h"
#import "OpenGLProperties.h"

@implementation GLContextBlock

@synthesize EAGLApi;

- (void)setupContext:(OpenGLProperties*)aGLProp {   
    if ([_children count] > 0) {
        Block *child = [[self getOrderedChildren] objectAtIndex: 0];
        [child performSelector: child.operation withObject: self];
    }
    //o bloco filho é quem atribiu o EAGLApi
    if (EAGLApi > 0) {
        EAGLContext *xContext = [[EAGLContext alloc] initWithAPI: EAGLApi]; //criando o contexto com a versão 2.0 da OpenGL ES
        if (!xContext) {
            NSLog(@"Failed to initialize OpenGLES context"); //se tentar rodar em um device que não suporta esta versão escreve log
        }
        
        if (![EAGLContext setCurrentContext:xContext]) { //seta como contexto corrente
            NSLog(@"Failed to set current OpenGL context"); //se não conseguir por qualquer motivo, gera log
        }
        aGLProp.context = xContext;
    }
}

- (void)removeChild:(Block*)aChild:(Connector*)aConnector {
    //tem que zerar a EAGLApi, pois o filho está sendo removido
    EAGLApi = 0;
    [super removeChild:aChild :aConnector];
}

- (id)init:(CGPoint)aLocation:(CGPoint)aCenter {
    self = [super init :aLocation :aCenter];
    if (self) {
        _operation = @selector(setupContext:);
    }
    return self;
}

- (BOOL)canReceiveThisAsChild:(Block*)aChild {
    return aChild.getClassName == @"OpenGLES2Block";
}

- (BOOL)canReceiveThisAsSibling:(Block*)aSibling {
    return aSibling.getClassName == @"GLProgramBlock";
}

- (NSString*)getClassName {
    return @"GLContextBlock";
}

- (NSString*)getReadableCode {
    return @"EAGLContext *xContext = [[EAGLContext alloc] initWithAPI: EAGLApi]; \n"
            "if (!xContext) { \n"
            "    NSLog(@\"Failed to initialize OpenGLES context\"); \n"
            "} \n"
            "if (![EAGLContext setCurrentContext:xContext]) { \n"
            "    NSLog(@\"Failed to set current OpenGL context\"); \n"
            "}";
}

- (NSString*)getName {
    return @"GLContext";
}


@end
