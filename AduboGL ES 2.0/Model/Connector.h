//
//  Connector.h
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 14/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Connector : NSObject

- (id)init:(double)aX:(double)aY;

@property (readwrite) double x;
@property (readwrite) double y;
@property (readwrite) BOOL used;
@property (assign) NSObject *block;

@end
