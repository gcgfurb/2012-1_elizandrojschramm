//
//  VertexShaderBlock.m
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 15/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "VertexShaderBlock.h"
#import "OpenGLProperties.h"

@implementation VertexShaderBlock

- (void)generateVertexCode:(OpenGLProperties*)aGLProp {
    aGLProp.vertexShaderStr =  @"attribute vec4 Position;                              \n"
                                "attribute vec4 SourceColor;                           \n"
                                "varying vec4 DestinationColor;                        \n"
                                "uniform mat4 Projection;                              \n"
                                "uniform mat4 Modelview;                               \n"
                                "void main(void) {                                     \n"
                                "    DestinationColor = SourceColor;                   \n"
                                "    gl_Position = Projection * Modelview * Position;  \n"
                                "}";    
}


- (id)init:(CGPoint)aLocation:(CGPoint)aCenter {
    self = [super init :aLocation :aCenter];
    if (self) {
        _operation = @selector(generateVertexCode:);
    }
    return self;
}

- (NSString*)getClassName {
    return @"VertexShaderBlock";
}

- (NSString*)getReadableCode {
    return @"attribute vec4 Position;                              \n"
            "attribute vec4 SourceColor;                           \n"
            "varying vec4 DestinationColor;                        \n"
            "uniform mat4 Projection;                              \n"
            "uniform mat4 Modelview;                               \n"
            "void main(void) {                                     \n"
            "    DestinationColor = SourceColor;                   \n"
            "    gl_Position = Projection * Modelview * Position;  \n"
            "}";  
}

- (NSString*)getName {
    return @"Vertex Shader";
}


@end
