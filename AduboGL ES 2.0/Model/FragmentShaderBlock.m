//
//  FragmentShaderBlock.m
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 15/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "FragmentShaderBlock.h"
#import "OpenGLProperties.h"

@implementation FragmentShaderBlock

- (void)generateFragmentCode:(OpenGLProperties*)aGLProp {
    aGLProp.fragmentShaderSrt = @"varying lowp vec4 DestinationColor;  \n"
                                 "void main(void) {                    \n"
                                 "    gl_FragColor = DestinationColor; \n"
                                 "}";
}

- (id)init:(CGPoint)aLocation:(CGPoint)aCenter {
    self = [super init :aLocation :aCenter];
    if (self) {
        _operation = @selector(generateFragmentCode:);
    }
    return self;
}

- (NSString*)getClassName {
    return @"FragmentShaderBlock";
}

- (NSString*)getReadableCode {
    return @"varying lowp vec4 DestinationColor;  \n"
            "void main(void) {                    \n"
            "    gl_FragColor = DestinationColor; \n"
            "}";
}

- (NSString*)getName {
    return @"Fragment Shader";
}

@end
