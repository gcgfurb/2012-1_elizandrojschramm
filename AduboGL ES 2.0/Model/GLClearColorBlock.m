//
//  GLClearColorBlock.m
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 15/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "GLClearColorBlock.h"

@implementation GLClearColorBlock {
    float red, green, blue;
}

- (void)defineGLGlearColor {
    glClearColor(red, green, blue, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}


- (id)init:(CGPoint)aLocation:(CGPoint)aCenter {
    self = [super init :aLocation :aCenter];
    if (self) {
        _operation = @selector(defineGLGlearColor);
        _sequence = 2;
        red = 0;
        green = 0;
        blue = 0;
    }
    return self;
}

- (NSString*)getClassName {
    return @"GLClearColorBlock";
}

- (void)setColor:(float)aRed:(float)aGreen:(float)aBlue {
    red = aRed;
    green = aGreen;
    blue = aBlue;
}

- (float)getRed {
    return red;
}

- (float)getGreen {
    return green;
}

- (float)getBlue {
    return blue;
}

- (NSString*)getReadableCode {
    NSString* line1 = [NSString stringWithFormat: @"glClearColor(%.1f, %.1f, %.1f, 1.0); \n", red, green, blue];
    return [line1 stringByAppendingString:@"glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);"];
}

- (NSString*)getName {
    return @"GLClearColor";
}

- (void)saveToXML:(NSMutableString*)aXMLElement {
    NSString *xValues = [NSString stringWithFormat:@"<block><center>{%.2f,%.2f}</center><class>%@</class><params>"
                         "%.2f,%.2f,%.2f</params></block>", self.center.x, self.center.y, [self getClassName], red, green, blue];
    [aXMLElement appendString:xValues];
}


@end







