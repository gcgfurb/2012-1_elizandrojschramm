//
//  Code.h
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 24/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OpenGLProperties : NSObject

- (id)init;

@property (retain) EAGLContext* context;
@property (readwrite) GLuint programHandle;
@property (retain) NSString* vertexShaderStr;
@property (retain) NSString* fragmentShaderSrt;
@property (readwrite) GLuint positionSlot;
@property (readwrite) GLuint colorSlot;
@property (readwrite) GLuint projectionUniform;
@property (readwrite) GLuint modelViewUniform;

@end
