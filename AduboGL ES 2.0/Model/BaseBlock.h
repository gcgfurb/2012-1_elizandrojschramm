//
//  BaseBlock.h
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 14/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "Block.h"
#import "Connector.h"

@interface BaseBlock : Block {
    NSMutableArray *_children, *_siblings;
}

- (void)addChild:(Block*)aChild;
- (void)removeChild:(Block*)aChild:(Connector*)aConnector;
- (BOOL)canReceiveThisAsChild:(Block*)aChild;
- (void)addClamp:(Connector*)aClamp;
- (Connector*)getClamp:(int)aIndex;
- (Connector*)getRelativeClamp:(int)aIndex;
- (Connector*)getClampByXY:(double)aX:(double)aY;
- (NSArray*)getOrderedChildren;
- (void)addSibling:(Block*)aSibling;
- (void)removeSibling:(Block*)aSibling;
- (BOOL)canReceiveThisAsSibling:(Block*)aSibling;

/**
 * Lista dos engates.
 **/
@property NSMutableArray *clamps;
/**
 * Lista de blocos filhos.
 **/
@property NSMutableArray *children;
/**
 * Lista de blocos irmãos.
 **/
@property NSMutableArray *siblings;

@end
