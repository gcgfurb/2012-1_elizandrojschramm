//
//  GLClearColorBlock.h
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 15/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "Block.h"

@interface GLClearColorBlock : Block

- (void)setColor:(float)aRed:(float)aGreen:(float)aBlue;
- (float)getRed;
- (float)getGreen;
- (float)getBlue;

@end
