//
//  LinkAttributesBlock.m
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 15/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import "LinkAttributesBlock.h"
#import "OpenGLProperties.h"

@implementation LinkAttributesBlock

- (void)setAttributes:(OpenGLProperties*)aGLProp {
    if (aGLProp.programHandle < INT32_MAX) {
        //pega um ponteiro para as variáveis de entrada - serão usadas durante todo o programa
        aGLProp.positionSlot = glGetAttribLocation(aGLProp.programHandle, "Position");
        aGLProp.colorSlot = glGetAttribLocation(aGLProp.programHandle, "SourceColor");
        //habilita o array, pois por padrão eles vêm desabilitados
        glEnableVertexAttribArray(aGLProp.positionSlot);
        glEnableVertexAttribArray(aGLProp.colorSlot);
        //pega referência para as matrizes que são comuns a todos os vértices
        aGLProp.projectionUniform = glGetUniformLocation(aGLProp.programHandle, "Projection");
        aGLProp.modelViewUniform = glGetUniformLocation(aGLProp.programHandle, "Modelview");
    }
}

- (id)init:(CGPoint)aLocation:(CGPoint)aCenter {
    self = [super init :aLocation :aCenter];
    if (self) {
        _operation = @selector(setAttributes:);
        _sequence = 2; //tem que ser chamado depois do LinkGLProgramBlock
    }
    return self;
}

- (NSString*)getClassName {
    return @"LinkAttributesBlock";
}

- (NSString*)getReadableCode {
    return @"positionSlot = glGetAttribLocation(programHandle, \"Position\"); \n"
            "colorSlot = glGetAttribLocation(programHandle, \"SourceColor\"); \n"
            "glEnableVertexAttribArray(positionSlot); \n"
            "glEnableVertexAttribArray(colorSlot); \n"
            "projectionUniform = glGetUniformLocation(programHandle, \"Projection\"); \n"
            "modelViewUniform = glGetUniformLocation(programHandle, \"Modelview\");";
}

- (NSString*)getName {
    return @"Vincular atributos";
}

@end
