//
//  MatrixUtil.h
//  AduboGL ES 2.0
//
//  Created by Elizandro Schramm on 21/05/12.
//  Copyright (c) 2012 Elizandro José Schramm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MatrixUtil : NSObject

+ (void)mtxLoadIdentity:(float*)mtx;
+ (void)mtxLoadPerspective:(float*)mtx:(float)fov:(float)aspect:(float)nearZ:(float)farZ;
+ (void)mtxTranslateApply:(float*)mtx:(float)xTrans:(float)yTrans:(float)zTrans;
+ (void)mtxRotateXApply:(float*)mtx:(float)deg;
+ (void)mtxRotateYApply:(float*)mtx:(float)deg;

@end
